-- Vhdl test bench created from schematic C:\Plis\l_4\l_sh.sch - Sun Dec 24 17:54:53 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY l_sh_l_sh_sch_tb IS
END l_sh_l_sh_sch_tb;
ARCHITECTURE behavioral OF l_sh_l_sh_sch_tb IS 

   COMPONENT l_sh
   PORT( a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          ce	:	IN	STD_LOGIC; 
          clk	:	IN	STD_LOGIC; 
          clr	:	IN	STD_LOGIC; 
          res1	:	OUT	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          res3	:	OUT	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          res	:	OUT	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          res2	:	OUT	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          ZERO	:	IN	STD_LOGIC; 
          ONE	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL a	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL b	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL ce	:	STD_LOGIC:= '1';
   SIGNAL clk	:	STD_LOGIC:= '1';
   SIGNAL clr	:	STD_LOGIC:= '0';
   SIGNAL res1	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL res3	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL res	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL c	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL res2	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL ZERO	:	STD_LOGIC:= '0';
   SIGNAL ONE	:	STD_LOGIC:= '1';
	constant clk_c : time :=10 ns;
	SIGNAL buf : integer :=0;

BEGIN

   UUT: l_sh PORT MAP(
		a => a, 
		b => b, 
		ce => ce, 
		clk => clk, 
		clr => clr, 
		res1 => res1, 
		res3 => res3, 
		res => res, 
		c => c, 
		res2 => res2, 
		ZERO => ZERO, 
		ONE => ONE
   );

tb : PROCESS
   BEGIN
	a <= "00000001"; 
	b <= "00000001"; 
	c <= "00000110"; 
	for i in 0 to 10 loop 
	clk <= '0'; 
	wait for clk_c; 
	buf <= buf + 1; 

	clk <= '1'; 
	wait for clk_c; 
	a <= std_logic_vector(unsigned(a) + (1)); 
	b <= std_logic_vector(unsigned(b) + (1)); 
	c <= std_logic_vector(unsigned(c) + (1)); 
	end loop;
	
   END PROCESS;

END;
