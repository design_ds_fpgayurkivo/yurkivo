<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="a(7:0)" />
        <signal name="c(7:0)" />
        <signal name="counter(3:0)" />
        <signal name="Control(23:0)" />
        <signal name="CLK" />
        <signal name="ZERO_bit" />
        <signal name="CE" />
        <signal name="res(31:0)" />
        <signal name="XLXN_26(31:0)" />
        <signal name="XLXN_27(31:0)" />
        <signal name="Control(0)" />
        <signal name="Control(1)" />
        <signal name="Control(2)" />
        <signal name="Control(3)" />
        <signal name="XLXN_33(31:0)" />
        <signal name="XLXN_34(31:0)" />
        <signal name="XLXN_35(31:0)" />
        <signal name="XLXN_36(31:0)" />
        <signal name="Control(4)" />
        <signal name="Control(5)" />
        <signal name="Control(6)" />
        <signal name="Control(10:7)" />
        <signal name="Control(11)" />
        <signal name="Control(13:12)" />
        <signal name="Control(17:14)" />
        <signal name="Control(18)" />
        <signal name="Control(20:19)" />
        <signal name="res2(31:0)" />
        <signal name="res1(31:0)" />
        <signal name="d(7:0)" />
        <signal name="res3(31:0)" />
        <signal name="res4(31:0)" />
        <signal name="XLXN_113(31:0)" />
        <signal name="XLXN_114(31:0)" />
        <signal name="XLXN_115(31:0)" />
        <port polarity="Input" name="a(7:0)" />
        <port polarity="Input" name="c(7:0)" />
        <port polarity="Output" name="counter(3:0)" />
        <port polarity="Output" name="Control(23:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="ZERO_bit" />
        <port polarity="Input" name="CE" />
        <port polarity="Output" name="res(31:0)" />
        <port polarity="Output" name="res2(31:0)" />
        <port polarity="Output" name="res1(31:0)" />
        <port polarity="Input" name="d(7:0)" />
        <port polarity="Output" name="res3(31:0)" />
        <port polarity="Output" name="res4(31:0)" />
        <blockdef name="conv8_32">
            <timestamp>2017-12-18T16:43:52</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="MUX_4_32">
            <timestamp>2017-12-18T16:40:11</timestamp>
            <rect width="304" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-364" height="24" />
            <line x2="432" y1="-352" y2="-352" x1="368" />
        </blockdef>
        <blockdef name="counter">
            <timestamp>2017-12-18T16:58:12</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="208" y2="208" x1="0" />
            <line x2="544" y1="144" y2="144" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="rom">
            <timestamp>2017-12-26T22:36:38</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="mux32">
            <timestamp>2017-12-26T12:57:51</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="addersub">
            <timestamp>2017-12-18T16:53:59</timestamp>
            <rect width="224" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="112" y2="112" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" x1="0" />
            <line x2="32" y1="176" y2="176" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="256" y1="112" y2="112" style="linewidth:W" x1="288" />
        </blockdef>
        <blockdef name="mul32">
            <timestamp>2017-12-18T16:51:30</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="32" y1="240" y2="240" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="register32">
            <timestamp>2017-12-18T16:45:42</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="ram">
            <timestamp>2017-12-26T21:29:33</timestamp>
            <rect width="256" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-364" height="24" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
        </blockdef>
        <blockdef name="ram2">
            <timestamp>2017-12-26T21:31:47</timestamp>
            <rect width="256" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-364" height="24" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
        </blockdef>
        <block symbolname="conv8_32" name="XLXI_1">
            <blockpin signalname="a(7:0)" name="in_sig(7:0)" />
            <blockpin signalname="XLXN_113(31:0)" name="out_sig(31:0)" />
        </block>
        <block symbolname="conv8_32" name="XLXI_3">
            <blockpin signalname="c(7:0)" name="in_sig(7:0)" />
            <blockpin signalname="XLXN_115(31:0)" name="out_sig(31:0)" />
        </block>
        <block symbolname="MUX_4_32" name="XLXI_9">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(20:19)" name="s0(1:0)" />
            <blockpin name="data1(31:0)" />
            <blockpin signalname="XLXN_114(31:0)" name="data2(31:0)" />
            <blockpin signalname="res3(31:0)" name="data3(31:0)" />
            <blockpin name="data4(31:0)" />
            <blockpin signalname="XLXN_27(31:0)" name="dara_res(31:0)" />
        </block>
        <block symbolname="MUX_4_32" name="XLXI_12">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(13:12)" name="s0(1:0)" />
            <blockpin signalname="XLXN_115(31:0)" name="data1(31:0)" />
            <blockpin signalname="XLXN_113(31:0)" name="data2(31:0)" />
            <blockpin signalname="res3(31:0)" name="data3(31:0)" />
            <blockpin signalname="res4(31:0)" name="data4(31:0)" />
            <blockpin signalname="XLXN_26(31:0)" name="dara_res(31:0)" />
        </block>
        <block symbolname="counter" name="XLXI_13">
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="counter(3:0)" name="q(3:0)" />
        </block>
        <block symbolname="rom" name="XLXI_14">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="counter(3:0)" name="A(3:0)" />
            <blockpin signalname="Control(23:0)" name="D(23:0)" />
        </block>
        <block symbolname="mux32" name="XLXI_15">
            <blockpin signalname="Control(0)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_36(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="mux32" name="XLXI_16">
            <blockpin signalname="Control(1)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_35(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="mux32" name="XLXI_17">
            <blockpin signalname="Control(2)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_34(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="mux32" name="XLXI_18">
            <blockpin signalname="Control(3)" name="s0" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="res1(31:0)" name="data1(31:0)" />
            <blockpin signalname="res2(31:0)" name="data2(31:0)" />
            <blockpin signalname="XLXN_33(31:0)" name="data_o(31:0)" />
        </block>
        <block symbolname="addersub" name="XLXI_19">
            <blockpin signalname="XLXN_34(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_33(31:0)" name="b(31:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="Control(4)" name="add" />
            <blockpin signalname="CE" name="ce" />
            <blockpin signalname="res4(31:0)" name="s(31:0)" />
        </block>
        <block symbolname="mul32" name="XLXI_20">
            <blockpin signalname="XLXN_36(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_35(31:0)" name="b(31:0)" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="res3(31:0)" name="p(31:0)" />
        </block>
        <block symbolname="register32" name="XLXI_21">
            <blockpin signalname="Control(6)" name="ld" />
            <blockpin signalname="Control(5)" name="clr" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="res2(31:0)" name="d(31:0)" />
            <blockpin signalname="res(31:0)" name="q(31:0)" />
        </block>
        <block symbolname="ram" name="XLXI_23">
            <blockpin signalname="ZERO_bit" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(18)" name="OE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(17:14)" name="A(3:0)" />
            <blockpin signalname="XLXN_27(31:0)" name="DI(31:0)" />
            <blockpin signalname="res1(31:0)" name="DQ(31:0)" />
        </block>
        <block symbolname="conv8_32" name="XLXI_26">
            <blockpin signalname="d(7:0)" name="in_sig(7:0)" />
            <blockpin signalname="XLXN_114(31:0)" name="out_sig(31:0)" />
        </block>
        <block symbolname="ram2" name="XLXI_27">
            <blockpin signalname="ZERO_bit" name="WE" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="Control(11)" name="OE" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Control(10:7)" name="A(3:0)" />
            <blockpin signalname="XLXN_26(31:0)" name="DI(31:0)" />
            <blockpin signalname="res2(31:0)" name="DQ(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <branch name="a(7:0)">
            <wire x2="944" y1="1040" y2="1040" x1="624" />
        </branch>
        <branch name="c(7:0)">
            <wire x2="944" y1="1184" y2="1184" x1="608" />
            <wire x2="960" y1="1184" y2="1184" x1="944" />
        </branch>
        <instance x="1616" y="960" name="XLXI_9" orien="R0">
        </instance>
        <instance x="2960" y="960" name="XLXI_12" orien="R0">
        </instance>
        <instance x="416" y="448" name="XLXI_13" orien="R0">
        </instance>
        <instance x="1280" y="2272" name="XLXI_15" orien="R0">
        </instance>
        <instance x="1280" y="2656" name="XLXI_16" orien="R0">
        </instance>
        <instance x="1280" y="3024" name="XLXI_17" orien="R0">
        </instance>
        <instance x="1280" y="3424" name="XLXI_18" orien="R0">
        </instance>
        <instance x="2096" y="2896" name="XLXI_19" orien="R0">
        </instance>
        <instance x="2000" y="2112" name="XLXI_20" orien="R0">
        </instance>
        <instance x="4880" y="1168" name="XLXI_21" orien="R0">
        </instance>
        <branch name="counter(3:0)">
            <wire x2="448" y1="240" y2="240" x1="400" />
            <wire x2="400" y1="240" y2="416" x1="400" />
            <wire x2="1056" y1="416" y2="416" x1="400" />
            <wire x2="1136" y1="416" y2="416" x1="1056" />
            <wire x2="1056" y1="416" y2="592" x1="1056" />
            <wire x2="1056" y1="592" y2="592" x1="992" />
        </branch>
        <iomarker fontsize="28" x="1136" y="416" name="counter(3:0)" orien="R0" />
        <branch name="Control(23:0)">
            <wire x2="1536" y1="176" y2="176" x1="832" />
            <wire x2="2128" y1="176" y2="176" x1="1536" />
            <wire x2="2192" y1="176" y2="176" x1="2128" />
            <wire x2="2912" y1="176" y2="176" x1="2192" />
            <wire x2="3504" y1="176" y2="176" x1="2912" />
            <wire x2="3568" y1="176" y2="176" x1="3504" />
            <wire x2="4384" y1="176" y2="176" x1="3568" />
            <wire x2="4384" y1="176" y2="944" x1="4384" />
            <wire x2="4384" y1="944" y2="1008" x1="4384" />
            <wire x2="4384" y1="1008" y2="3808" x1="4384" />
            <wire x2="1072" y1="3808" y2="3808" x1="864" />
            <wire x2="1136" y1="3808" y2="3808" x1="1072" />
            <wire x2="1216" y1="3808" y2="3808" x1="1136" />
            <wire x2="1280" y1="3808" y2="3808" x1="1216" />
            <wire x2="2000" y1="3808" y2="3808" x1="1280" />
            <wire x2="4384" y1="3808" y2="3808" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="864" y="3808" name="Control(23:0)" orien="R180" />
        <branch name="CLK">
            <wire x2="288" y1="656" y2="656" x1="224" />
            <wire x2="416" y1="656" y2="656" x1="288" />
            <wire x2="288" y1="656" y2="2112" x1="288" />
            <wire x2="1280" y1="2112" y2="2112" x1="288" />
            <wire x2="288" y1="2112" y2="2496" x1="288" />
            <wire x2="1280" y1="2496" y2="2496" x1="288" />
            <wire x2="288" y1="2496" y2="2864" x1="288" />
            <wire x2="1280" y1="2864" y2="2864" x1="288" />
            <wire x2="288" y1="2864" y2="3264" x1="288" />
            <wire x2="288" y1="3264" y2="3888" x1="288" />
            <wire x2="1856" y1="3888" y2="3888" x1="288" />
            <wire x2="1936" y1="3888" y2="3888" x1="1856" />
            <wire x2="4368" y1="3888" y2="3888" x1="1936" />
            <wire x2="4368" y1="3888" y2="3904" x1="4368" />
            <wire x2="1280" y1="3264" y2="3264" x1="288" />
            <wire x2="448" y1="176" y2="176" x1="288" />
            <wire x2="288" y1="176" y2="656" x1="288" />
            <wire x2="1584" y1="352" y2="608" x1="1584" />
            <wire x2="1616" y1="608" y2="608" x1="1584" />
            <wire x2="2160" y1="352" y2="352" x1="1584" />
            <wire x2="2160" y1="352" y2="800" x1="2160" />
            <wire x2="2288" y1="800" y2="800" x1="2160" />
            <wire x2="2864" y1="352" y2="352" x1="2160" />
            <wire x2="2864" y1="352" y2="608" x1="2864" />
            <wire x2="2960" y1="608" y2="608" x1="2864" />
            <wire x2="3488" y1="352" y2="352" x1="2864" />
            <wire x2="4352" y1="352" y2="352" x1="3488" />
            <wire x2="4352" y1="352" y2="1072" x1="4352" />
            <wire x2="4880" y1="1072" y2="1072" x1="4352" />
            <wire x2="4352" y1="1072" y2="3904" x1="4352" />
            <wire x2="4368" y1="3904" y2="3904" x1="4352" />
            <wire x2="3488" y1="352" y2="800" x1="3488" />
            <wire x2="3584" y1="800" y2="800" x1="3488" />
            <wire x2="2000" y1="2352" y2="2352" x1="1856" />
            <wire x2="1856" y1="2352" y2="3888" x1="1856" />
            <wire x2="2096" y1="3040" y2="3040" x1="1936" />
            <wire x2="1936" y1="3040" y2="3888" x1="1936" />
        </branch>
        <iomarker fontsize="28" x="224" y="656" name="CLK" orien="R180" />
        <branch name="ZERO_bit">
            <wire x2="2256" y1="304" y2="304" x1="2048" />
            <wire x2="2256" y1="304" y2="608" x1="2256" />
            <wire x2="2288" y1="608" y2="608" x1="2256" />
            <wire x2="3456" y1="304" y2="304" x1="2256" />
            <wire x2="3456" y1="304" y2="608" x1="3456" />
            <wire x2="3584" y1="608" y2="608" x1="3456" />
        </branch>
        <iomarker fontsize="28" x="2048" y="304" name="ZERO_bit" orien="R180" />
        <branch name="CE">
            <wire x2="2208" y1="432" y2="432" x1="1952" />
            <wire x2="2208" y1="432" y2="672" x1="2208" />
            <wire x2="2288" y1="672" y2="672" x1="2208" />
            <wire x2="3536" y1="432" y2="432" x1="2208" />
            <wire x2="3536" y1="432" y2="672" x1="3536" />
            <wire x2="3584" y1="672" y2="672" x1="3536" />
            <wire x2="4304" y1="432" y2="432" x1="3536" />
            <wire x2="4304" y1="432" y2="3840" x1="4304" />
            <wire x2="2096" y1="3136" y2="3136" x1="2048" />
            <wire x2="2048" y1="3136" y2="3840" x1="2048" />
            <wire x2="4304" y1="3840" y2="3840" x1="2048" />
        </branch>
        <iomarker fontsize="28" x="1952" y="432" name="CE" orien="R180" />
        <branch name="res(31:0)">
            <wire x2="5456" y1="944" y2="944" x1="5264" />
        </branch>
        <iomarker fontsize="28" x="5456" y="944" name="res(31:0)" orien="R0" />
        <branch name="XLXN_26(31:0)">
            <wire x2="3440" y1="608" y2="608" x1="3392" />
            <wire x2="3440" y1="608" y2="928" x1="3440" />
            <wire x2="3584" y1="928" y2="928" x1="3440" />
        </branch>
        <branch name="XLXN_27(31:0)">
            <wire x2="2144" y1="608" y2="608" x1="2048" />
            <wire x2="2144" y1="608" y2="928" x1="2144" />
            <wire x2="2288" y1="928" y2="928" x1="2144" />
        </branch>
        <bustap x2="1072" y1="3808" y2="3712" x1="1072" />
        <bustap x2="1136" y1="3808" y2="3712" x1="1136" />
        <bustap x2="1216" y1="3808" y2="3712" x1="1216" />
        <bustap x2="1280" y1="3808" y2="3712" x1="1280" />
        <branch name="Control(0)">
            <wire x2="1280" y1="2048" y2="2048" x1="1072" />
            <wire x2="1072" y1="2048" y2="3712" x1="1072" />
        </branch>
        <branch name="Control(1)">
            <wire x2="1280" y1="2432" y2="2432" x1="1136" />
            <wire x2="1136" y1="2432" y2="3712" x1="1136" />
        </branch>
        <branch name="Control(2)">
            <wire x2="1280" y1="2800" y2="2800" x1="1216" />
            <wire x2="1216" y1="2800" y2="3712" x1="1216" />
        </branch>
        <branch name="Control(3)">
            <wire x2="1280" y1="3200" y2="3200" x1="1264" />
            <wire x2="1264" y1="3200" y2="3696" x1="1264" />
            <wire x2="1280" y1="3696" y2="3696" x1="1264" />
            <wire x2="1280" y1="3696" y2="3712" x1="1280" />
        </branch>
        <branch name="XLXN_33(31:0)">
            <wire x2="1872" y1="3200" y2="3200" x1="1664" />
            <wire x2="1872" y1="3008" y2="3200" x1="1872" />
            <wire x2="2096" y1="3008" y2="3008" x1="1872" />
        </branch>
        <branch name="XLXN_34(31:0)">
            <wire x2="1872" y1="2800" y2="2800" x1="1664" />
            <wire x2="1872" y1="2800" y2="2976" x1="1872" />
            <wire x2="2096" y1="2976" y2="2976" x1="1872" />
        </branch>
        <branch name="XLXN_35(31:0)">
            <wire x2="1824" y1="2432" y2="2432" x1="1664" />
            <wire x2="1824" y1="2256" y2="2432" x1="1824" />
            <wire x2="2000" y1="2256" y2="2256" x1="1824" />
        </branch>
        <branch name="XLXN_36(31:0)">
            <wire x2="1824" y1="2048" y2="2048" x1="1664" />
            <wire x2="1824" y1="2048" y2="2192" x1="1824" />
            <wire x2="2000" y1="2192" y2="2192" x1="1824" />
        </branch>
        <bustap x2="2000" y1="3808" y2="3712" x1="2000" />
        <branch name="Control(4)">
            <wire x2="2096" y1="3072" y2="3072" x1="2000" />
            <wire x2="2000" y1="3072" y2="3712" x1="2000" />
        </branch>
        <bustap x2="4480" y1="1008" y2="1008" x1="4384" />
        <bustap x2="4480" y1="944" y2="944" x1="4384" />
        <branch name="Control(5)">
            <wire x2="4880" y1="1008" y2="1008" x1="4480" />
        </branch>
        <branch name="Control(6)">
            <wire x2="4880" y1="944" y2="944" x1="4480" />
        </branch>
        <instance x="448" y="272" name="XLXI_14" orien="R0">
        </instance>
        <bustap x2="3568" y1="176" y2="272" x1="3568" />
        <bustap x2="3504" y1="176" y2="272" x1="3504" />
        <branch name="Control(10:7)">
            <wire x2="3552" y1="272" y2="864" x1="3552" />
            <wire x2="3584" y1="864" y2="864" x1="3552" />
            <wire x2="3568" y1="272" y2="272" x1="3552" />
        </branch>
        <branch name="Control(11)">
            <wire x2="3504" y1="272" y2="736" x1="3504" />
            <wire x2="3584" y1="736" y2="736" x1="3504" />
        </branch>
        <bustap x2="2912" y1="176" y2="272" x1="2912" />
        <branch name="Control(13:12)">
            <wire x2="2912" y1="272" y2="672" x1="2912" />
            <wire x2="2960" y1="672" y2="672" x1="2912" />
        </branch>
        <bustap x2="2192" y1="176" y2="272" x1="2192" />
        <bustap x2="2128" y1="176" y2="272" x1="2128" />
        <branch name="Control(17:14)">
            <wire x2="2192" y1="272" y2="864" x1="2192" />
            <wire x2="2288" y1="864" y2="864" x1="2192" />
        </branch>
        <branch name="Control(18)">
            <wire x2="2128" y1="272" y2="736" x1="2128" />
            <wire x2="2288" y1="736" y2="736" x1="2128" />
        </branch>
        <bustap x2="1536" y1="176" y2="272" x1="1536" />
        <branch name="Control(20:19)">
            <wire x2="1536" y1="272" y2="672" x1="1536" />
            <wire x2="1616" y1="672" y2="672" x1="1536" />
        </branch>
        <instance x="2288" y="960" name="XLXI_23" orien="R0">
        </instance>
        <branch name="res2(31:0)">
            <wire x2="896" y1="1712" y2="1744" x1="896" />
            <wire x2="4000" y1="1744" y2="1744" x1="896" />
            <wire x2="1088" y1="1712" y2="1712" x1="896" />
            <wire x2="1088" y1="1712" y2="2240" x1="1088" />
            <wire x2="1088" y1="2240" y2="2624" x1="1088" />
            <wire x2="1088" y1="2624" y2="2992" x1="1088" />
            <wire x2="1088" y1="2992" y2="3392" x1="1088" />
            <wire x2="1280" y1="3392" y2="3392" x1="1088" />
            <wire x2="1280" y1="2992" y2="2992" x1="1088" />
            <wire x2="1280" y1="2624" y2="2624" x1="1088" />
            <wire x2="1280" y1="2240" y2="2240" x1="1088" />
            <wire x2="4000" y1="1584" y2="1584" x1="3808" />
            <wire x2="4000" y1="1584" y2="1744" x1="4000" />
            <wire x2="4000" y1="608" y2="608" x1="3968" />
            <wire x2="4336" y1="608" y2="608" x1="4000" />
            <wire x2="4336" y1="608" y2="1136" x1="4336" />
            <wire x2="4880" y1="1136" y2="1136" x1="4336" />
            <wire x2="4000" y1="608" y2="1584" x1="4000" />
        </branch>
        <branch name="res1(31:0)">
            <wire x2="1904" y1="1568" y2="1568" x1="1152" />
            <wire x2="1152" y1="1568" y2="2176" x1="1152" />
            <wire x2="1152" y1="2176" y2="2560" x1="1152" />
            <wire x2="1152" y1="2560" y2="2928" x1="1152" />
            <wire x2="1152" y1="2928" y2="3328" x1="1152" />
            <wire x2="1280" y1="3328" y2="3328" x1="1152" />
            <wire x2="1280" y1="2928" y2="2928" x1="1152" />
            <wire x2="1280" y1="2560" y2="2560" x1="1152" />
            <wire x2="1280" y1="2176" y2="2176" x1="1152" />
            <wire x2="1904" y1="1536" y2="1568" x1="1904" />
            <wire x2="2688" y1="1536" y2="1536" x1="1904" />
            <wire x2="2688" y1="608" y2="608" x1="2672" />
            <wire x2="2688" y1="608" y2="1488" x1="2688" />
            <wire x2="2688" y1="1488" y2="1536" x1="2688" />
            <wire x2="2816" y1="1488" y2="1488" x1="2688" />
        </branch>
        <iomarker fontsize="28" x="2816" y="1488" name="res1(31:0)" orien="R0" />
        <iomarker fontsize="28" x="3808" y="1584" name="res2(31:0)" orien="R180" />
        <instance x="944" y="1072" name="XLXI_1" orien="R0">
        </instance>
        <branch name="d(7:0)">
            <wire x2="944" y1="1328" y2="1328" x1="736" />
            <wire x2="960" y1="1328" y2="1328" x1="944" />
        </branch>
        <iomarker fontsize="28" x="624" y="1040" name="a(7:0)" orien="R180" />
        <branch name="res3(31:0)">
            <wire x2="1408" y1="112" y2="864" x1="1408" />
            <wire x2="1616" y1="864" y2="864" x1="1408" />
            <wire x2="2720" y1="112" y2="112" x1="1408" />
            <wire x2="4080" y1="112" y2="112" x1="2720" />
            <wire x2="4192" y1="112" y2="112" x1="4080" />
            <wire x2="4192" y1="112" y2="2192" x1="4192" />
            <wire x2="2720" y1="112" y2="864" x1="2720" />
            <wire x2="2960" y1="864" y2="864" x1="2720" />
            <wire x2="3440" y1="2192" y2="2192" x1="2576" />
            <wire x2="4192" y1="2192" y2="2192" x1="3440" />
            <wire x2="3440" y1="2096" y2="2096" x1="3424" />
            <wire x2="3440" y1="2096" y2="2192" x1="3440" />
        </branch>
        <branch name="res4(31:0)">
            <wire x2="3424" y1="3008" y2="3008" x1="2384" />
            <wire x2="4224" y1="3008" y2="3008" x1="3424" />
            <wire x2="4240" y1="3008" y2="3008" x1="4224" />
            <wire x2="2800" y1="256" y2="336" x1="2800" />
            <wire x2="3648" y1="336" y2="336" x1="2800" />
            <wire x2="2880" y1="256" y2="256" x1="2800" />
            <wire x2="2880" y1="256" y2="928" x1="2880" />
            <wire x2="2960" y1="928" y2="928" x1="2880" />
            <wire x2="3424" y1="2832" y2="3008" x1="3424" />
            <wire x2="3648" y1="240" y2="336" x1="3648" />
            <wire x2="4240" y1="240" y2="240" x1="3648" />
            <wire x2="4240" y1="240" y2="3008" x1="4240" />
        </branch>
        <iomarker fontsize="28" x="3424" y="2096" name="res3(31:0)" orien="R180" />
        <iomarker fontsize="28" x="3424" y="2832" name="res4(31:0)" orien="R270" />
        <instance x="960" y="1216" name="XLXI_3" orien="R0">
        </instance>
        <iomarker fontsize="28" x="608" y="1184" name="c(7:0)" orien="R180" />
        <instance x="960" y="1360" name="XLXI_26" orien="R0">
        </instance>
        <iomarker fontsize="28" x="736" y="1328" name="d(7:0)" orien="R180" />
        <branch name="XLXN_113(31:0)">
            <wire x2="2752" y1="1040" y2="1040" x1="1328" />
            <wire x2="2752" y1="800" y2="1040" x1="2752" />
            <wire x2="2960" y1="800" y2="800" x1="2752" />
        </branch>
        <branch name="XLXN_114(31:0)">
            <wire x2="1472" y1="1328" y2="1328" x1="1344" />
            <wire x2="1472" y1="800" y2="1328" x1="1472" />
            <wire x2="1616" y1="800" y2="800" x1="1472" />
        </branch>
        <branch name="XLXN_115(31:0)">
            <wire x2="2736" y1="1184" y2="1184" x1="1344" />
            <wire x2="2736" y1="736" y2="1184" x1="2736" />
            <wire x2="2960" y1="736" y2="736" x1="2736" />
        </branch>
        <instance x="3584" y="960" name="XLXI_27" orien="R0">
        </instance>
    </sheet>
</drawing>