-- Vhdl test bench created from schematic C:\Plis\l_4\sh2.sch - Tue Dec 26 23:34:23 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY sh2_sh2_sch_tb IS
END sh2_sh2_sch_tb;
ARCHITECTURE behavioral OF sh2_sh2_sch_tb IS 

   COMPONENT sh2
   PORT( a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          counter	:	OUT	STD_LOGIC_VECTOR (3 DOWNTO 0); 
          Control	:	OUT	STD_LOGIC_VECTOR (23 DOWNTO 0); 
          CLK	:	IN	STD_LOGIC; 
          ZERO_bit	:	IN	STD_LOGIC; 
          CE	:	IN	STD_LOGIC; 
          res	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          res2	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          res1	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          d	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          res3	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          res4	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0));
   END COMPONENT;

   SIGNAL a	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"01";
   SIGNAL c	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"01";
	SIGNAL d	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"01";
   SIGNAL counter	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL Control	:	STD_LOGIC_VECTOR (23 DOWNTO 0);
   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL ZERO_bit	:	STD_LOGIC:='0';
   SIGNAL CE	:	STD_LOGIC:='1';
   SIGNAL res	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL res2	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL res1	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL res3	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL res4	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
	constant clk_c : time := 5 ns;

BEGIN

   UUT: sh2 PORT MAP(
		a => a, 
		c => c, 
		counter => counter, 
		Control => Control, 
		CLK => CLK, 
		ZERO_bit => ZERO_bit, 
		CE => CE, 
		res => res, 
		res2 => res2, 
		res1 => res1, 
		d => d, 
		res3 => res3, 
		res4 => res4
   );
CLC_process :process

	begin
	CLK <= '1';
	wait for clk_c/2;
		
	CLK <= '0';
	wait for clk_c/2;
	end process;

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
