----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:23:18 12/24/2017 
-- Design Name: 
-- Module Name:    conv - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity conv is
    Port ( s8 : in  STD_LOGIC_VECTOR (7 downto 0);
           s16 : out  STD_LOGIC_VECTOR (15 downto 0));
end conv;

architecture Behavioral of conv is

begin
 s16 <= X"00" & s8;


end Behavioral;

