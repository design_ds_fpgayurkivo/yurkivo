----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:46:24 12/18/2017 
-- Design Name: 
-- Module Name:    rom - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.std_logic_arith.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rom is
    Port ( CLK : in  STD_LOGIC;
           A : in  STD_LOGIC_VECTOR (3 downto 0);
           D : out  STD_LOGIC_VECTOR (23 downto 0));
end rom;

architecture Behavioral of rom is

begin
	process (A)
		variable A_temp:integer;
	begin	   
		A_temp:= conv_integer(A(3 downto 0));	
				case A_temp is
					when 0 => D <= x"084030";	   				   				
					when 1 => D <= x"000833";	   				   				
					when 2 => D <= x"1090B0";     
					when 3 => D <= x"0C58B8";     
					when 4 => D <= x"0031B0";
					when 5 => D <= x"14B9B2";
					when 6 => D <= x"002130";  	  			
					when 7 => D <= x"042928"; 
 	  			   when 8 => D <= x"0031B0";
					when 9 => D <= x"0039D0"; 
					when others => D <= "ZZZZZZZZZZZZZZZZZZZZZZZZ";
				end case;			
	end process;

end Behavioral;

