////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: ip.v
// /___/   /\     Timestamp: Sun Dec 24 17:26:33 2017
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog C:/Plis/l_4/ipcore_dir/tmp/_cg/ip.ngc C:/Plis/l_4/ipcore_dir/tmp/_cg/ip.v 
// Device	: 6vlx75tff484-3
// Input file	: C:/Plis/l_4/ipcore_dir/tmp/_cg/ip.ngc
// Output file	: C:/Plis/l_4/ipcore_dir/tmp/_cg/ip.v
// # of Modules	: 1
// Design Name	: ip
// Xilinx        : D:\Xilinx\14.7\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module ip (
  clk, a, b, p
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input [7 : 0] a;
  input [7 : 0] b;
  output [7 : 0] p;
  
  // synthesis translate_off
  
  wire \blk00000001/sig000000bb ;
  wire \blk00000001/sig000000ba ;
  wire \blk00000001/sig000000b9 ;
  wire \blk00000001/sig000000b8 ;
  wire \blk00000001/sig000000b7 ;
  wire \blk00000001/sig000000b6 ;
  wire \blk00000001/sig000000b5 ;
  wire \blk00000001/sig000000b4 ;
  wire \blk00000001/sig000000b3 ;
  wire \blk00000001/sig000000b2 ;
  wire \blk00000001/sig000000b1 ;
  wire \blk00000001/sig000000b0 ;
  wire \blk00000001/sig000000af ;
  wire \blk00000001/sig000000ae ;
  wire \blk00000001/sig000000ad ;
  wire \blk00000001/sig000000ac ;
  wire \blk00000001/sig000000ab ;
  wire \blk00000001/sig000000aa ;
  wire \blk00000001/sig000000a9 ;
  wire \blk00000001/sig000000a8 ;
  wire \blk00000001/sig000000a7 ;
  wire \blk00000001/sig000000a6 ;
  wire \blk00000001/sig000000a5 ;
  wire \blk00000001/sig000000a4 ;
  wire \blk00000001/sig000000a3 ;
  wire \blk00000001/sig000000a2 ;
  wire \blk00000001/sig000000a1 ;
  wire \blk00000001/sig000000a0 ;
  wire \blk00000001/sig0000009f ;
  wire \blk00000001/sig0000009e ;
  wire \blk00000001/sig0000009d ;
  wire \blk00000001/sig0000009c ;
  wire \blk00000001/sig0000009b ;
  wire \blk00000001/sig0000009a ;
  wire \blk00000001/sig00000099 ;
  wire \blk00000001/sig00000098 ;
  wire \blk00000001/sig00000097 ;
  wire \blk00000001/sig00000096 ;
  wire \blk00000001/sig00000095 ;
  wire \blk00000001/sig00000094 ;
  wire \blk00000001/sig00000093 ;
  wire \blk00000001/sig00000092 ;
  wire \blk00000001/sig00000091 ;
  wire \blk00000001/sig00000090 ;
  wire \blk00000001/sig0000008f ;
  wire \blk00000001/sig0000008e ;
  wire \blk00000001/sig0000008d ;
  wire \blk00000001/sig0000008c ;
  wire \blk00000001/sig0000008b ;
  wire \blk00000001/sig0000008a ;
  wire \blk00000001/sig00000089 ;
  wire \blk00000001/sig00000088 ;
  wire \blk00000001/sig00000087 ;
  wire \blk00000001/sig00000086 ;
  wire \blk00000001/sig00000085 ;
  wire \blk00000001/sig00000084 ;
  wire \blk00000001/sig00000083 ;
  wire \blk00000001/sig00000082 ;
  wire \blk00000001/sig00000081 ;
  wire \blk00000001/sig00000080 ;
  wire \blk00000001/sig0000007f ;
  wire \blk00000001/sig0000007e ;
  wire \blk00000001/sig0000007d ;
  wire \blk00000001/sig0000007c ;
  wire \blk00000001/sig0000007b ;
  wire \blk00000001/sig0000007a ;
  wire \blk00000001/sig00000079 ;
  wire \blk00000001/sig00000078 ;
  wire \blk00000001/sig00000077 ;
  wire \blk00000001/sig00000076 ;
  wire \blk00000001/sig00000075 ;
  wire \blk00000001/sig00000074 ;
  wire \blk00000001/sig00000073 ;
  wire \blk00000001/sig00000072 ;
  wire \blk00000001/sig00000071 ;
  wire \blk00000001/sig00000070 ;
  wire \blk00000001/sig0000006f ;
  wire \blk00000001/sig0000006e ;
  wire \blk00000001/sig0000006d ;
  wire \blk00000001/sig0000006c ;
  wire \blk00000001/sig0000006b ;
  wire \blk00000001/sig0000006a ;
  wire \blk00000001/sig00000069 ;
  wire \blk00000001/sig00000068 ;
  wire \blk00000001/sig00000067 ;
  wire \blk00000001/sig00000066 ;
  wire \blk00000001/sig00000065 ;
  wire \blk00000001/sig00000064 ;
  wire \blk00000001/sig00000063 ;
  wire \blk00000001/sig00000062 ;
  wire \blk00000001/sig00000061 ;
  wire \blk00000001/sig00000060 ;
  wire \blk00000001/sig0000005f ;
  wire \blk00000001/sig0000005e ;
  wire \blk00000001/sig0000005d ;
  wire \blk00000001/sig0000005c ;
  wire \blk00000001/sig0000005b ;
  wire \blk00000001/sig0000005a ;
  wire \blk00000001/sig00000059 ;
  wire \blk00000001/sig00000058 ;
  wire \blk00000001/sig00000057 ;
  wire \blk00000001/sig00000056 ;
  wire \blk00000001/sig00000055 ;
  wire \blk00000001/sig00000054 ;
  wire \blk00000001/sig00000053 ;
  wire \blk00000001/sig00000052 ;
  wire \blk00000001/sig00000051 ;
  wire \blk00000001/sig00000050 ;
  wire \blk00000001/sig0000004f ;
  wire \blk00000001/sig0000004e ;
  wire \blk00000001/sig0000004d ;
  wire \blk00000001/sig0000004c ;
  wire \blk00000001/sig0000004b ;
  wire \blk00000001/sig0000004a ;
  wire \blk00000001/sig00000049 ;
  wire \blk00000001/sig00000048 ;
  wire \blk00000001/sig00000047 ;
  wire \blk00000001/sig00000046 ;
  wire \blk00000001/sig00000045 ;
  wire \blk00000001/sig00000044 ;
  wire \blk00000001/sig00000043 ;
  wire \blk00000001/sig00000042 ;
  wire \blk00000001/sig00000041 ;
  wire \blk00000001/sig00000040 ;
  wire \blk00000001/sig0000003f ;
  wire \blk00000001/sig0000003e ;
  wire \blk00000001/sig0000003d ;
  wire \blk00000001/sig0000003c ;
  wire \blk00000001/sig0000003b ;
  wire \blk00000001/sig0000003a ;
  wire \blk00000001/sig00000039 ;
  wire \blk00000001/sig00000038 ;
  wire \blk00000001/sig00000037 ;
  wire \blk00000001/sig00000036 ;
  wire \blk00000001/sig00000035 ;
  wire \blk00000001/sig00000034 ;
  wire \blk00000001/sig00000033 ;
  wire \blk00000001/sig00000032 ;
  wire \blk00000001/sig00000031 ;
  wire \blk00000001/sig00000030 ;
  wire \blk00000001/sig0000002f ;
  wire \blk00000001/sig0000002e ;
  wire \blk00000001/sig0000002d ;
  wire \blk00000001/sig0000002c ;
  wire \blk00000001/sig0000002b ;
  wire \blk00000001/sig0000002a ;
  wire \blk00000001/sig00000029 ;
  wire \blk00000001/sig00000028 ;
  wire \blk00000001/sig00000027 ;
  wire \blk00000001/sig00000026 ;
  wire \blk00000001/sig00000025 ;
  wire \blk00000001/sig00000024 ;
  wire \blk00000001/sig00000023 ;
  wire \blk00000001/sig00000022 ;
  wire \blk00000001/sig00000021 ;
  wire \blk00000001/sig00000020 ;
  wire \blk00000001/sig0000001f ;
  wire \blk00000001/sig0000001e ;
  wire \blk00000001/sig0000001d ;
  wire \blk00000001/sig0000001c ;
  wire \blk00000001/sig0000001b ;
  wire \blk00000001/sig0000001a ;
  wire \NLW_blk00000001/blk0000001f_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk0000001b_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000018_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000017_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000014_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000013_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000011_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000010_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk0000000f_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk0000000d_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk0000000c_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk0000000b_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk0000000a_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000009_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000008_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000007_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000006_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000005_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000004_O_UNCONNECTED ;
  wire \NLW_blk00000001/blk00000003_O_UNCONNECTED ;
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000bf  (
    .I0(a[0]),
    .I1(b[0]),
    .O(\blk00000001/sig000000bb )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000be  (
    .I0(a[0]),
    .I1(b[2]),
    .O(\blk00000001/sig000000b8 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000bd  (
    .I0(a[0]),
    .I1(b[4]),
    .O(\blk00000001/sig000000b5 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000bc  (
    .I0(a[0]),
    .I1(b[6]),
    .O(\blk00000001/sig000000b2 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000bb  (
    .I0(a[0]),
    .I1(b[1]),
    .I2(a[1]),
    .I3(b[0]),
    .O(\blk00000001/sig0000006a )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000ba  (
    .I0(a[1]),
    .I1(b[1]),
    .I2(a[2]),
    .I3(b[0]),
    .O(\blk00000001/sig00000060 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b9  (
    .I0(a[2]),
    .I1(b[1]),
    .I2(a[3]),
    .I3(b[0]),
    .O(\blk00000001/sig00000059 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b8  (
    .I0(a[3]),
    .I1(b[1]),
    .I2(a[4]),
    .I3(b[0]),
    .O(\blk00000001/sig00000053 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b7  (
    .I0(a[4]),
    .I1(b[1]),
    .I2(a[5]),
    .I3(b[0]),
    .O(\blk00000001/sig0000004d )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b6  (
    .I0(a[5]),
    .I1(b[1]),
    .I2(a[6]),
    .I3(b[0]),
    .O(\blk00000001/sig00000048 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b5  (
    .I0(a[6]),
    .I1(b[1]),
    .I2(a[7]),
    .I3(b[0]),
    .O(\blk00000001/sig00000043 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000b4  (
    .I0(a[7]),
    .I1(b[1]),
    .O(\blk00000001/sig0000003f )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b3  (
    .I0(a[0]),
    .I1(b[3]),
    .I2(a[1]),
    .I3(b[2]),
    .O(\blk00000001/sig00000067 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b2  (
    .I0(a[1]),
    .I1(b[3]),
    .I2(a[2]),
    .I3(b[2]),
    .O(\blk00000001/sig0000005e )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b1  (
    .I0(a[2]),
    .I1(b[3]),
    .I2(a[3]),
    .I3(b[2]),
    .O(\blk00000001/sig00000057 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000b0  (
    .I0(a[3]),
    .I1(b[3]),
    .I2(a[4]),
    .I3(b[2]),
    .O(\blk00000001/sig00000051 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000af  (
    .I0(a[4]),
    .I1(b[3]),
    .I2(a[5]),
    .I3(b[2]),
    .O(\blk00000001/sig0000004b )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000ae  (
    .I0(a[5]),
    .I1(b[3]),
    .I2(a[6]),
    .I3(b[2]),
    .O(\blk00000001/sig00000046 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000ad  (
    .I0(a[6]),
    .I1(b[3]),
    .I2(a[7]),
    .I3(b[2]),
    .O(\blk00000001/sig00000042 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000ac  (
    .I0(a[7]),
    .I1(b[3]),
    .O(\blk00000001/sig0000003e )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000ab  (
    .I0(a[0]),
    .I1(b[5]),
    .I2(a[1]),
    .I3(b[4]),
    .O(\blk00000001/sig00000064 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000aa  (
    .I0(a[1]),
    .I1(b[5]),
    .I2(a[2]),
    .I3(b[4]),
    .O(\blk00000001/sig0000005c )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a9  (
    .I0(a[2]),
    .I1(b[5]),
    .I2(a[3]),
    .I3(b[4]),
    .O(\blk00000001/sig00000055 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a8  (
    .I0(a[3]),
    .I1(b[5]),
    .I2(a[4]),
    .I3(b[4]),
    .O(\blk00000001/sig0000004f )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a7  (
    .I0(a[4]),
    .I1(b[5]),
    .I2(a[5]),
    .I3(b[4]),
    .O(\blk00000001/sig0000004a )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a6  (
    .I0(a[5]),
    .I1(b[5]),
    .I2(a[6]),
    .I3(b[4]),
    .O(\blk00000001/sig00000045 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a5  (
    .I0(a[6]),
    .I1(b[5]),
    .I2(a[7]),
    .I3(b[4]),
    .O(\blk00000001/sig00000041 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000a4  (
    .I0(a[7]),
    .I1(b[5]),
    .O(\blk00000001/sig0000003d )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a3  (
    .I0(a[0]),
    .I1(b[7]),
    .I2(a[1]),
    .I3(b[6]),
    .O(\blk00000001/sig00000061 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a2  (
    .I0(a[1]),
    .I1(b[7]),
    .I2(a[2]),
    .I3(b[6]),
    .O(\blk00000001/sig0000005a )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a1  (
    .I0(a[2]),
    .I1(b[7]),
    .I2(a[3]),
    .I3(b[6]),
    .O(\blk00000001/sig00000054 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk000000a0  (
    .I0(a[3]),
    .I1(b[7]),
    .I2(a[4]),
    .I3(b[6]),
    .O(\blk00000001/sig0000004e )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk0000009f  (
    .I0(a[4]),
    .I1(b[7]),
    .I2(a[5]),
    .I3(b[6]),
    .O(\blk00000001/sig00000049 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk0000009e  (
    .I0(a[5]),
    .I1(b[7]),
    .I2(a[6]),
    .I3(b[6]),
    .O(\blk00000001/sig00000044 )
  );
  LUT4 #(
    .INIT ( 16'h7888 ))
  \blk00000001/blk0000009d  (
    .I0(a[6]),
    .I1(b[7]),
    .I2(a[7]),
    .I3(b[6]),
    .O(\blk00000001/sig00000040 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000009c  (
    .I0(a[7]),
    .I1(b[7]),
    .O(\blk00000001/sig0000003c )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009b  (
    .C(clk),
    .D(\blk00000001/sig00000092 ),
    .Q(p[0])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009a  (
    .C(clk),
    .D(\blk00000001/sig0000006b ),
    .Q(p[1])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000099  (
    .C(clk),
    .D(\blk00000001/sig00000036 ),
    .Q(p[2])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000098  (
    .C(clk),
    .D(\blk00000001/sig00000037 ),
    .Q(p[3])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000097  (
    .C(clk),
    .D(\blk00000001/sig00000030 ),
    .Q(p[4])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000096  (
    .C(clk),
    .D(\blk00000001/sig00000031 ),
    .Q(p[5])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000095  (
    .C(clk),
    .D(\blk00000001/sig00000032 ),
    .Q(p[6])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000094  (
    .C(clk),
    .D(\blk00000001/sig00000033 ),
    .Q(p[7])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000093  (
    .I0(\blk00000001/sig00000069 ),
    .I1(\blk00000001/sig0000008f ),
    .O(\blk00000001/sig0000002f )
  );
  MUXCY   \blk00000001/blk00000092  (
    .CI(\blk00000001/sig0000001a ),
    .DI(\blk00000001/sig00000069 ),
    .S(\blk00000001/sig0000002f ),
    .O(\blk00000001/sig0000002e )
  );
  XORCY   \blk00000001/blk00000091  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig0000002f ),
    .O(\blk00000001/sig00000036 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000090  (
    .I0(\blk00000001/sig0000005f ),
    .I1(\blk00000001/sig00000068 ),
    .O(\blk00000001/sig0000002d )
  );
  MUXCY   \blk00000001/blk0000008f  (
    .CI(\blk00000001/sig0000002e ),
    .DI(\blk00000001/sig0000005f ),
    .S(\blk00000001/sig0000002d ),
    .O(\blk00000001/sig0000002c )
  );
  XORCY   \blk00000001/blk0000008e  (
    .CI(\blk00000001/sig0000002e ),
    .LI(\blk00000001/sig0000002d ),
    .O(\blk00000001/sig00000037 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000008d  (
    .I0(\blk00000001/sig00000058 ),
    .I1(\blk00000001/sig00000066 ),
    .O(\blk00000001/sig0000002b )
  );
  MUXCY   \blk00000001/blk0000008c  (
    .CI(\blk00000001/sig0000002c ),
    .DI(\blk00000001/sig00000058 ),
    .S(\blk00000001/sig0000002b ),
    .O(\blk00000001/sig0000002a )
  );
  XORCY   \blk00000001/blk0000008b  (
    .CI(\blk00000001/sig0000002c ),
    .LI(\blk00000001/sig0000002b ),
    .O(\blk00000001/sig00000038 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000008a  (
    .I0(\blk00000001/sig00000052 ),
    .I1(\blk00000001/sig0000005d ),
    .O(\blk00000001/sig00000029 )
  );
  MUXCY   \blk00000001/blk00000089  (
    .CI(\blk00000001/sig0000002a ),
    .DI(\blk00000001/sig00000052 ),
    .S(\blk00000001/sig00000029 ),
    .O(\blk00000001/sig00000028 )
  );
  XORCY   \blk00000001/blk00000088  (
    .CI(\blk00000001/sig0000002a ),
    .LI(\blk00000001/sig00000029 ),
    .O(\blk00000001/sig00000039 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000087  (
    .I0(\blk00000001/sig0000004c ),
    .I1(\blk00000001/sig00000056 ),
    .O(\blk00000001/sig00000027 )
  );
  MUXCY   \blk00000001/blk00000086  (
    .CI(\blk00000001/sig00000028 ),
    .DI(\blk00000001/sig0000004c ),
    .S(\blk00000001/sig00000027 ),
    .O(\blk00000001/sig00000026 )
  );
  XORCY   \blk00000001/blk00000085  (
    .CI(\blk00000001/sig00000028 ),
    .LI(\blk00000001/sig00000027 ),
    .O(\blk00000001/sig0000003a )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000084  (
    .I0(\blk00000001/sig00000047 ),
    .I1(\blk00000001/sig00000050 ),
    .O(\blk00000001/sig00000025 )
  );
  XORCY   \blk00000001/blk00000083  (
    .CI(\blk00000001/sig00000026 ),
    .LI(\blk00000001/sig00000025 ),
    .O(\blk00000001/sig0000003b )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000082  (
    .I0(\blk00000001/sig00000063 ),
    .I1(\blk00000001/sig00000089 ),
    .O(\blk00000001/sig00000024 )
  );
  MUXCY   \blk00000001/blk00000081  (
    .CI(\blk00000001/sig0000001a ),
    .DI(\blk00000001/sig00000063 ),
    .S(\blk00000001/sig00000024 ),
    .O(\blk00000001/sig00000023 )
  );
  XORCY   \blk00000001/blk00000080  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig00000024 ),
    .O(\blk00000001/sig00000034 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000007f  (
    .I0(\blk00000001/sig0000005b ),
    .I1(\blk00000001/sig00000062 ),
    .O(\blk00000001/sig00000022 )
  );
  XORCY   \blk00000001/blk0000007e  (
    .CI(\blk00000001/sig00000023 ),
    .LI(\blk00000001/sig00000022 ),
    .O(\blk00000001/sig00000035 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000007d  (
    .I0(\blk00000001/sig00000038 ),
    .I1(\blk00000001/sig0000008c ),
    .O(\blk00000001/sig00000021 )
  );
  MUXCY   \blk00000001/blk0000007c  (
    .CI(\blk00000001/sig0000001a ),
    .DI(\blk00000001/sig00000038 ),
    .S(\blk00000001/sig00000021 ),
    .O(\blk00000001/sig00000020 )
  );
  XORCY   \blk00000001/blk0000007b  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig00000021 ),
    .O(\blk00000001/sig00000030 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000007a  (
    .I0(\blk00000001/sig00000039 ),
    .I1(\blk00000001/sig00000065 ),
    .O(\blk00000001/sig0000001f )
  );
  MUXCY   \blk00000001/blk00000079  (
    .CI(\blk00000001/sig00000020 ),
    .DI(\blk00000001/sig00000039 ),
    .S(\blk00000001/sig0000001f ),
    .O(\blk00000001/sig0000001e )
  );
  XORCY   \blk00000001/blk00000078  (
    .CI(\blk00000001/sig00000020 ),
    .LI(\blk00000001/sig0000001f ),
    .O(\blk00000001/sig00000031 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000077  (
    .I0(\blk00000001/sig0000003a ),
    .I1(\blk00000001/sig00000034 ),
    .O(\blk00000001/sig0000001d )
  );
  MUXCY   \blk00000001/blk00000076  (
    .CI(\blk00000001/sig0000001e ),
    .DI(\blk00000001/sig0000003a ),
    .S(\blk00000001/sig0000001d ),
    .O(\blk00000001/sig0000001c )
  );
  XORCY   \blk00000001/blk00000075  (
    .CI(\blk00000001/sig0000001e ),
    .LI(\blk00000001/sig0000001d ),
    .O(\blk00000001/sig00000032 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000074  (
    .I0(\blk00000001/sig0000003b ),
    .I1(\blk00000001/sig00000035 ),
    .O(\blk00000001/sig0000001b )
  );
  XORCY   \blk00000001/blk00000073  (
    .CI(\blk00000001/sig0000001c ),
    .LI(\blk00000001/sig0000001b ),
    .O(\blk00000001/sig00000033 )
  );
  MULT_AND   \blk00000001/blk00000072  (
    .I0(b[0]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000ba )
  );
  MULT_AND   \blk00000001/blk00000071  (
    .I0(b[1]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000b9 )
  );
  MULT_AND   \blk00000001/blk00000070  (
    .I0(b[2]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000b7 )
  );
  MULT_AND   \blk00000001/blk0000006f  (
    .I0(b[3]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000b6 )
  );
  MULT_AND   \blk00000001/blk0000006e  (
    .I0(b[4]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000b4 )
  );
  MULT_AND   \blk00000001/blk0000006d  (
    .I0(b[5]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000b3 )
  );
  MULT_AND   \blk00000001/blk0000006c  (
    .I0(b[6]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000b1 )
  );
  MULT_AND   \blk00000001/blk0000006b  (
    .I0(b[7]),
    .I1(a[0]),
    .LO(\blk00000001/sig000000b0 )
  );
  MULT_AND   \blk00000001/blk0000006a  (
    .I0(b[1]),
    .I1(a[1]),
    .LO(\blk00000001/sig000000af )
  );
  MULT_AND   \blk00000001/blk00000069  (
    .I0(b[3]),
    .I1(a[1]),
    .LO(\blk00000001/sig000000ae )
  );
  MULT_AND   \blk00000001/blk00000068  (
    .I0(b[5]),
    .I1(a[1]),
    .LO(\blk00000001/sig000000ad )
  );
  MULT_AND   \blk00000001/blk00000067  (
    .I0(b[7]),
    .I1(a[1]),
    .LO(\blk00000001/sig000000ac )
  );
  MULT_AND   \blk00000001/blk00000066  (
    .I0(b[1]),
    .I1(a[2]),
    .LO(\blk00000001/sig000000ab )
  );
  MULT_AND   \blk00000001/blk00000065  (
    .I0(b[3]),
    .I1(a[2]),
    .LO(\blk00000001/sig000000aa )
  );
  MULT_AND   \blk00000001/blk00000064  (
    .I0(b[5]),
    .I1(a[2]),
    .LO(\blk00000001/sig000000a9 )
  );
  MULT_AND   \blk00000001/blk00000063  (
    .I0(b[7]),
    .I1(a[2]),
    .LO(\blk00000001/sig000000a8 )
  );
  MULT_AND   \blk00000001/blk00000062  (
    .I0(b[1]),
    .I1(a[3]),
    .LO(\blk00000001/sig000000a7 )
  );
  MULT_AND   \blk00000001/blk00000061  (
    .I0(b[3]),
    .I1(a[3]),
    .LO(\blk00000001/sig000000a6 )
  );
  MULT_AND   \blk00000001/blk00000060  (
    .I0(b[5]),
    .I1(a[3]),
    .LO(\blk00000001/sig000000a5 )
  );
  MULT_AND   \blk00000001/blk0000005f  (
    .I0(b[7]),
    .I1(a[3]),
    .LO(\blk00000001/sig000000a4 )
  );
  MULT_AND   \blk00000001/blk0000005e  (
    .I0(b[1]),
    .I1(a[4]),
    .LO(\blk00000001/sig000000a3 )
  );
  MULT_AND   \blk00000001/blk0000005d  (
    .I0(b[3]),
    .I1(a[4]),
    .LO(\blk00000001/sig000000a2 )
  );
  MULT_AND   \blk00000001/blk0000005c  (
    .I0(b[5]),
    .I1(a[4]),
    .LO(\blk00000001/sig000000a1 )
  );
  MULT_AND   \blk00000001/blk0000005b  (
    .I0(b[7]),
    .I1(a[4]),
    .LO(\blk00000001/sig000000a0 )
  );
  MULT_AND   \blk00000001/blk0000005a  (
    .I0(b[1]),
    .I1(a[5]),
    .LO(\blk00000001/sig0000009f )
  );
  MULT_AND   \blk00000001/blk00000059  (
    .I0(b[3]),
    .I1(a[5]),
    .LO(\blk00000001/sig0000009e )
  );
  MULT_AND   \blk00000001/blk00000058  (
    .I0(b[5]),
    .I1(a[5]),
    .LO(\blk00000001/sig0000009d )
  );
  MULT_AND   \blk00000001/blk00000057  (
    .I0(b[7]),
    .I1(a[5]),
    .LO(\blk00000001/sig0000009c )
  );
  MULT_AND   \blk00000001/blk00000056  (
    .I0(b[1]),
    .I1(a[6]),
    .LO(\blk00000001/sig0000009b )
  );
  MULT_AND   \blk00000001/blk00000055  (
    .I0(b[3]),
    .I1(a[6]),
    .LO(\blk00000001/sig0000009a )
  );
  MULT_AND   \blk00000001/blk00000054  (
    .I0(b[5]),
    .I1(a[6]),
    .LO(\blk00000001/sig00000099 )
  );
  MULT_AND   \blk00000001/blk00000053  (
    .I0(b[7]),
    .I1(a[6]),
    .LO(\blk00000001/sig00000098 )
  );
  MULT_AND   \blk00000001/blk00000052  (
    .I0(b[1]),
    .I1(a[7]),
    .LO(\blk00000001/sig00000097 )
  );
  MULT_AND   \blk00000001/blk00000051  (
    .I0(b[3]),
    .I1(a[7]),
    .LO(\blk00000001/sig00000096 )
  );
  MULT_AND   \blk00000001/blk00000050  (
    .I0(b[5]),
    .I1(a[7]),
    .LO(\blk00000001/sig00000095 )
  );
  MULT_AND   \blk00000001/blk0000004f  (
    .I0(b[7]),
    .I1(a[7]),
    .LO(\blk00000001/sig00000094 )
  );
  MUXCY   \blk00000001/blk0000004e  (
    .CI(\blk00000001/sig0000001a ),
    .DI(\blk00000001/sig000000ba ),
    .S(\blk00000001/sig000000bb ),
    .O(\blk00000001/sig00000093 )
  );
  XORCY   \blk00000001/blk0000004d  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig000000bb ),
    .O(\blk00000001/sig00000092 )
  );
  MUXCY   \blk00000001/blk0000004c  (
    .CI(\blk00000001/sig00000093 ),
    .DI(\blk00000001/sig000000b9 ),
    .S(\blk00000001/sig0000006a ),
    .O(\blk00000001/sig00000091 )
  );
  MUXCY   \blk00000001/blk0000004b  (
    .CI(\blk00000001/sig0000001a ),
    .DI(\blk00000001/sig000000b7 ),
    .S(\blk00000001/sig000000b8 ),
    .O(\blk00000001/sig00000090 )
  );
  XORCY   \blk00000001/blk0000004a  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig000000b8 ),
    .O(\blk00000001/sig0000008f )
  );
  MUXCY   \blk00000001/blk00000049  (
    .CI(\blk00000001/sig00000090 ),
    .DI(\blk00000001/sig000000b6 ),
    .S(\blk00000001/sig00000067 ),
    .O(\blk00000001/sig0000008e )
  );
  MUXCY   \blk00000001/blk00000048  (
    .CI(\blk00000001/sig0000001a ),
    .DI(\blk00000001/sig000000b4 ),
    .S(\blk00000001/sig000000b5 ),
    .O(\blk00000001/sig0000008d )
  );
  XORCY   \blk00000001/blk00000047  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig000000b5 ),
    .O(\blk00000001/sig0000008c )
  );
  MUXCY   \blk00000001/blk00000046  (
    .CI(\blk00000001/sig0000008d ),
    .DI(\blk00000001/sig000000b3 ),
    .S(\blk00000001/sig00000064 ),
    .O(\blk00000001/sig0000008b )
  );
  MUXCY   \blk00000001/blk00000045  (
    .CI(\blk00000001/sig0000001a ),
    .DI(\blk00000001/sig000000b1 ),
    .S(\blk00000001/sig000000b2 ),
    .O(\blk00000001/sig0000008a )
  );
  XORCY   \blk00000001/blk00000044  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig000000b2 ),
    .O(\blk00000001/sig00000089 )
  );
  MUXCY   \blk00000001/blk00000043  (
    .CI(\blk00000001/sig0000008a ),
    .DI(\blk00000001/sig000000b0 ),
    .S(\blk00000001/sig00000061 ),
    .O(\blk00000001/sig00000088 )
  );
  MUXCY   \blk00000001/blk00000042  (
    .CI(\blk00000001/sig00000091 ),
    .DI(\blk00000001/sig000000af ),
    .S(\blk00000001/sig00000060 ),
    .O(\blk00000001/sig00000087 )
  );
  MUXCY   \blk00000001/blk00000041  (
    .CI(\blk00000001/sig0000008e ),
    .DI(\blk00000001/sig000000ae ),
    .S(\blk00000001/sig0000005e ),
    .O(\blk00000001/sig00000086 )
  );
  MUXCY   \blk00000001/blk00000040  (
    .CI(\blk00000001/sig0000008b ),
    .DI(\blk00000001/sig000000ad ),
    .S(\blk00000001/sig0000005c ),
    .O(\blk00000001/sig00000085 )
  );
  MUXCY   \blk00000001/blk0000003f  (
    .CI(\blk00000001/sig00000088 ),
    .DI(\blk00000001/sig000000ac ),
    .S(\blk00000001/sig0000005a ),
    .O(\blk00000001/sig00000084 )
  );
  MUXCY   \blk00000001/blk0000003e  (
    .CI(\blk00000001/sig00000087 ),
    .DI(\blk00000001/sig000000ab ),
    .S(\blk00000001/sig00000059 ),
    .O(\blk00000001/sig00000083 )
  );
  MUXCY   \blk00000001/blk0000003d  (
    .CI(\blk00000001/sig00000086 ),
    .DI(\blk00000001/sig000000aa ),
    .S(\blk00000001/sig00000057 ),
    .O(\blk00000001/sig00000082 )
  );
  MUXCY   \blk00000001/blk0000003c  (
    .CI(\blk00000001/sig00000085 ),
    .DI(\blk00000001/sig000000a9 ),
    .S(\blk00000001/sig00000055 ),
    .O(\blk00000001/sig00000081 )
  );
  MUXCY   \blk00000001/blk0000003b  (
    .CI(\blk00000001/sig00000084 ),
    .DI(\blk00000001/sig000000a8 ),
    .S(\blk00000001/sig00000054 ),
    .O(\blk00000001/sig00000080 )
  );
  MUXCY   \blk00000001/blk0000003a  (
    .CI(\blk00000001/sig00000083 ),
    .DI(\blk00000001/sig000000a7 ),
    .S(\blk00000001/sig00000053 ),
    .O(\blk00000001/sig0000007f )
  );
  MUXCY   \blk00000001/blk00000039  (
    .CI(\blk00000001/sig00000082 ),
    .DI(\blk00000001/sig000000a6 ),
    .S(\blk00000001/sig00000051 ),
    .O(\blk00000001/sig0000007e )
  );
  MUXCY   \blk00000001/blk00000038  (
    .CI(\blk00000001/sig00000081 ),
    .DI(\blk00000001/sig000000a5 ),
    .S(\blk00000001/sig0000004f ),
    .O(\blk00000001/sig0000007d )
  );
  MUXCY   \blk00000001/blk00000037  (
    .CI(\blk00000001/sig00000080 ),
    .DI(\blk00000001/sig000000a4 ),
    .S(\blk00000001/sig0000004e ),
    .O(\blk00000001/sig0000007c )
  );
  MUXCY   \blk00000001/blk00000036  (
    .CI(\blk00000001/sig0000007f ),
    .DI(\blk00000001/sig000000a3 ),
    .S(\blk00000001/sig0000004d ),
    .O(\blk00000001/sig0000007b )
  );
  MUXCY   \blk00000001/blk00000035  (
    .CI(\blk00000001/sig0000007e ),
    .DI(\blk00000001/sig000000a2 ),
    .S(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000007a )
  );
  MUXCY   \blk00000001/blk00000034  (
    .CI(\blk00000001/sig0000007d ),
    .DI(\blk00000001/sig000000a1 ),
    .S(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig00000079 )
  );
  MUXCY   \blk00000001/blk00000033  (
    .CI(\blk00000001/sig0000007c ),
    .DI(\blk00000001/sig000000a0 ),
    .S(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000078 )
  );
  MUXCY   \blk00000001/blk00000032  (
    .CI(\blk00000001/sig0000007b ),
    .DI(\blk00000001/sig0000009f ),
    .S(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000077 )
  );
  MUXCY   \blk00000001/blk00000031  (
    .CI(\blk00000001/sig0000007a ),
    .DI(\blk00000001/sig0000009e ),
    .S(\blk00000001/sig00000046 ),
    .O(\blk00000001/sig00000076 )
  );
  MUXCY   \blk00000001/blk00000030  (
    .CI(\blk00000001/sig00000079 ),
    .DI(\blk00000001/sig0000009d ),
    .S(\blk00000001/sig00000045 ),
    .O(\blk00000001/sig00000075 )
  );
  MUXCY   \blk00000001/blk0000002f  (
    .CI(\blk00000001/sig00000078 ),
    .DI(\blk00000001/sig0000009c ),
    .S(\blk00000001/sig00000044 ),
    .O(\blk00000001/sig00000074 )
  );
  MUXCY   \blk00000001/blk0000002e  (
    .CI(\blk00000001/sig00000077 ),
    .DI(\blk00000001/sig0000009b ),
    .S(\blk00000001/sig00000043 ),
    .O(\blk00000001/sig00000073 )
  );
  MUXCY   \blk00000001/blk0000002d  (
    .CI(\blk00000001/sig00000076 ),
    .DI(\blk00000001/sig0000009a ),
    .S(\blk00000001/sig00000042 ),
    .O(\blk00000001/sig00000072 )
  );
  MUXCY   \blk00000001/blk0000002c  (
    .CI(\blk00000001/sig00000075 ),
    .DI(\blk00000001/sig00000099 ),
    .S(\blk00000001/sig00000041 ),
    .O(\blk00000001/sig00000071 )
  );
  MUXCY   \blk00000001/blk0000002b  (
    .CI(\blk00000001/sig00000074 ),
    .DI(\blk00000001/sig00000098 ),
    .S(\blk00000001/sig00000040 ),
    .O(\blk00000001/sig00000070 )
  );
  MUXCY   \blk00000001/blk0000002a  (
    .CI(\blk00000001/sig00000073 ),
    .DI(\blk00000001/sig00000097 ),
    .S(\blk00000001/sig0000003f ),
    .O(\blk00000001/sig0000006f )
  );
  MUXCY   \blk00000001/blk00000029  (
    .CI(\blk00000001/sig00000072 ),
    .DI(\blk00000001/sig00000096 ),
    .S(\blk00000001/sig0000003e ),
    .O(\blk00000001/sig0000006e )
  );
  MUXCY   \blk00000001/blk00000028  (
    .CI(\blk00000001/sig00000071 ),
    .DI(\blk00000001/sig00000095 ),
    .S(\blk00000001/sig0000003d ),
    .O(\blk00000001/sig0000006d )
  );
  MUXCY   \blk00000001/blk00000027  (
    .CI(\blk00000001/sig00000070 ),
    .DI(\blk00000001/sig00000094 ),
    .S(\blk00000001/sig0000003c ),
    .O(\blk00000001/sig0000006c )
  );
  XORCY   \blk00000001/blk00000026  (
    .CI(\blk00000001/sig00000093 ),
    .LI(\blk00000001/sig0000006a ),
    .O(\blk00000001/sig0000006b )
  );
  XORCY   \blk00000001/blk00000025  (
    .CI(\blk00000001/sig00000091 ),
    .LI(\blk00000001/sig00000060 ),
    .O(\blk00000001/sig00000069 )
  );
  XORCY   \blk00000001/blk00000024  (
    .CI(\blk00000001/sig00000090 ),
    .LI(\blk00000001/sig00000067 ),
    .O(\blk00000001/sig00000068 )
  );
  XORCY   \blk00000001/blk00000023  (
    .CI(\blk00000001/sig0000008e ),
    .LI(\blk00000001/sig0000005e ),
    .O(\blk00000001/sig00000066 )
  );
  XORCY   \blk00000001/blk00000022  (
    .CI(\blk00000001/sig0000008d ),
    .LI(\blk00000001/sig00000064 ),
    .O(\blk00000001/sig00000065 )
  );
  XORCY   \blk00000001/blk00000021  (
    .CI(\blk00000001/sig0000008b ),
    .LI(\blk00000001/sig0000005c ),
    .O(\blk00000001/sig00000063 )
  );
  XORCY   \blk00000001/blk00000020  (
    .CI(\blk00000001/sig0000008a ),
    .LI(\blk00000001/sig00000061 ),
    .O(\blk00000001/sig00000062 )
  );
  XORCY   \blk00000001/blk0000001f  (
    .CI(\blk00000001/sig00000088 ),
    .LI(\blk00000001/sig0000005a ),
    .O(\NLW_blk00000001/blk0000001f_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk0000001e  (
    .CI(\blk00000001/sig00000087 ),
    .LI(\blk00000001/sig00000059 ),
    .O(\blk00000001/sig0000005f )
  );
  XORCY   \blk00000001/blk0000001d  (
    .CI(\blk00000001/sig00000086 ),
    .LI(\blk00000001/sig00000057 ),
    .O(\blk00000001/sig0000005d )
  );
  XORCY   \blk00000001/blk0000001c  (
    .CI(\blk00000001/sig00000085 ),
    .LI(\blk00000001/sig00000055 ),
    .O(\blk00000001/sig0000005b )
  );
  XORCY   \blk00000001/blk0000001b  (
    .CI(\blk00000001/sig00000084 ),
    .LI(\blk00000001/sig00000054 ),
    .O(\NLW_blk00000001/blk0000001b_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk0000001a  (
    .CI(\blk00000001/sig00000083 ),
    .LI(\blk00000001/sig00000053 ),
    .O(\blk00000001/sig00000058 )
  );
  XORCY   \blk00000001/blk00000019  (
    .CI(\blk00000001/sig00000082 ),
    .LI(\blk00000001/sig00000051 ),
    .O(\blk00000001/sig00000056 )
  );
  XORCY   \blk00000001/blk00000018  (
    .CI(\blk00000001/sig00000081 ),
    .LI(\blk00000001/sig0000004f ),
    .O(\NLW_blk00000001/blk00000018_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000017  (
    .CI(\blk00000001/sig00000080 ),
    .LI(\blk00000001/sig0000004e ),
    .O(\NLW_blk00000001/blk00000017_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000016  (
    .CI(\blk00000001/sig0000007f ),
    .LI(\blk00000001/sig0000004d ),
    .O(\blk00000001/sig00000052 )
  );
  XORCY   \blk00000001/blk00000015  (
    .CI(\blk00000001/sig0000007e ),
    .LI(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig00000050 )
  );
  XORCY   \blk00000001/blk00000014  (
    .CI(\blk00000001/sig0000007d ),
    .LI(\blk00000001/sig0000004a ),
    .O(\NLW_blk00000001/blk00000014_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000013  (
    .CI(\blk00000001/sig0000007c ),
    .LI(\blk00000001/sig00000049 ),
    .O(\NLW_blk00000001/blk00000013_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000012  (
    .CI(\blk00000001/sig0000007b ),
    .LI(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig0000004c )
  );
  XORCY   \blk00000001/blk00000011  (
    .CI(\blk00000001/sig0000007a ),
    .LI(\blk00000001/sig00000046 ),
    .O(\NLW_blk00000001/blk00000011_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000010  (
    .CI(\blk00000001/sig00000079 ),
    .LI(\blk00000001/sig00000045 ),
    .O(\NLW_blk00000001/blk00000010_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk0000000f  (
    .CI(\blk00000001/sig00000078 ),
    .LI(\blk00000001/sig00000044 ),
    .O(\NLW_blk00000001/blk0000000f_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk0000000e  (
    .CI(\blk00000001/sig00000077 ),
    .LI(\blk00000001/sig00000043 ),
    .O(\blk00000001/sig00000047 )
  );
  XORCY   \blk00000001/blk0000000d  (
    .CI(\blk00000001/sig00000076 ),
    .LI(\blk00000001/sig00000042 ),
    .O(\NLW_blk00000001/blk0000000d_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk0000000c  (
    .CI(\blk00000001/sig00000075 ),
    .LI(\blk00000001/sig00000041 ),
    .O(\NLW_blk00000001/blk0000000c_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk0000000b  (
    .CI(\blk00000001/sig00000074 ),
    .LI(\blk00000001/sig00000040 ),
    .O(\NLW_blk00000001/blk0000000b_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk0000000a  (
    .CI(\blk00000001/sig00000073 ),
    .LI(\blk00000001/sig0000003f ),
    .O(\NLW_blk00000001/blk0000000a_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000009  (
    .CI(\blk00000001/sig00000072 ),
    .LI(\blk00000001/sig0000003e ),
    .O(\NLW_blk00000001/blk00000009_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000008  (
    .CI(\blk00000001/sig00000071 ),
    .LI(\blk00000001/sig0000003d ),
    .O(\NLW_blk00000001/blk00000008_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000007  (
    .CI(\blk00000001/sig00000070 ),
    .LI(\blk00000001/sig0000003c ),
    .O(\NLW_blk00000001/blk00000007_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000006  (
    .CI(\blk00000001/sig0000006f ),
    .LI(\blk00000001/sig0000001a ),
    .O(\NLW_blk00000001/blk00000006_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000005  (
    .CI(\blk00000001/sig0000006e ),
    .LI(\blk00000001/sig0000001a ),
    .O(\NLW_blk00000001/blk00000005_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000004  (
    .CI(\blk00000001/sig0000006d ),
    .LI(\blk00000001/sig0000001a ),
    .O(\NLW_blk00000001/blk00000004_O_UNCONNECTED )
  );
  XORCY   \blk00000001/blk00000003  (
    .CI(\blk00000001/sig0000006c ),
    .LI(\blk00000001/sig0000001a ),
    .O(\NLW_blk00000001/blk00000003_O_UNCONNECTED )
  );
  GND   \blk00000001/blk00000002  (
    .G(\blk00000001/sig0000001a )
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
