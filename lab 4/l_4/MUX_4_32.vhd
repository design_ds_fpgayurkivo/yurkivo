----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:39:33 12/18/2017 
-- Design Name: 
-- Module Name:    MUX_4_32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_4_32 is
    Port ( s0 : in  STD_LOGIC_VECTOR (1 downto 0);
           data1 : in  STD_LOGIC_VECTOR (31 downto 0);
           data2 : in  STD_LOGIC_VECTOR (31 downto 0);
           data3 : in  STD_LOGIC_VECTOR (31 downto 0);
           data4 : in  STD_LOGIC_VECTOR (31 downto 0);
           CLK : in  STD_LOGIC;
           dara_res : out  STD_LOGIC_VECTOR (31 downto 0));
end MUX_4_32;

architecture Behavioral of MUX_4_32 is
begin 
process(data1, data2, data3, data4, s0, CLK) 
begin 

case s0 is 

when "00" => dara_res <= data1; 
when "01" => dara_res <= data2; 
when "10" => dara_res <= data3; 
when "11" => dara_res <= data4; 
when others => dara_res <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"; 
end case; 
end process; 

end Behavioral;

