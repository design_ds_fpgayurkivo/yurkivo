<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(7:0)" />
        <signal name="XLXN_2(7:0)" />
        <signal name="XLXN_3(7:0)" />
        <signal name="XLXN_4(7:0)" />
        <signal name="L_A(7:0)" />
        <signal name="L_B(7:0)" />
        <signal name="L_P(15:0)" />
        <signal name="L_P2(15:0)" />
        <signal name="L_P1(15:0)" />
        <port polarity="Input" name="L_A(7:0)" />
        <port polarity="Input" name="L_B(7:0)" />
        <port polarity="Output" name="L_P(15:0)" />
        <port polarity="Output" name="L_P2(15:0)" />
        <port polarity="Output" name="L_P1(15:0)" />
        <blockdef name="l_M">
            <timestamp>2017-12-18T17:21:28</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="ip">
            <timestamp>2017-12-18T17:7:44</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="L_mM">
            <timestamp>2017-12-18T17:24:53</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="l_M" name="XLXI_1">
            <blockpin signalname="L_A(7:0)" name="L_A(7:0)" />
            <blockpin signalname="L_B(7:0)" name="L_B(7:0)" />
            <blockpin signalname="L_P(15:0)" name="L_P(15:0)" />
        </block>
        <block symbolname="ip" name="XLXI_2">
            <blockpin signalname="L_A(7:0)" name="a(7:0)" />
            <blockpin signalname="L_B(7:0)" name="b(7:0)" />
            <blockpin signalname="L_P2(15:0)" name="p(15:0)" />
        </block>
        <block symbolname="L_mM" name="XLXI_3">
            <blockpin signalname="L_A(7:0)" name="L_A(7:0)" />
            <blockpin signalname="L_B(7:0)" name="L_B(7:0)" />
            <blockpin signalname="L_P1(15:0)" name="L_p(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1536" y="640" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1520" y="1104" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1536" y="944" name="XLXI_3" orien="R0">
        </instance>
        <branch name="L_A(7:0)">
            <wire x2="1344" y1="544" y2="544" x1="1216" />
            <wire x2="1536" y1="544" y2="544" x1="1344" />
            <wire x2="1344" y1="544" y2="848" x1="1344" />
            <wire x2="1536" y1="848" y2="848" x1="1344" />
            <wire x2="1344" y1="848" y2="1184" x1="1344" />
            <wire x2="1520" y1="1184" y2="1184" x1="1344" />
        </branch>
        <branch name="L_B(7:0)">
            <wire x2="1408" y1="608" y2="608" x1="1216" />
            <wire x2="1536" y1="608" y2="608" x1="1408" />
            <wire x2="1408" y1="608" y2="912" x1="1408" />
            <wire x2="1536" y1="912" y2="912" x1="1408" />
            <wire x2="1408" y1="912" y2="1248" x1="1408" />
            <wire x2="1520" y1="1248" y2="1248" x1="1408" />
        </branch>
        <iomarker fontsize="28" x="1216" y="544" name="L_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="1216" y="608" name="L_B(7:0)" orien="R180" />
        <branch name="L_P(15:0)">
            <wire x2="2128" y1="544" y2="544" x1="1920" />
        </branch>
        <branch name="L_P2(15:0)">
            <wire x2="2256" y1="1184" y2="1184" x1="2096" />
        </branch>
        <branch name="L_P1(15:0)">
            <wire x2="2048" y1="848" y2="848" x1="1920" />
        </branch>
        <iomarker fontsize="28" x="2128" y="544" name="L_P(15:0)" orien="R0" />
        <iomarker fontsize="28" x="2048" y="848" name="L_P1(15:0)" orien="R0" />
        <iomarker fontsize="28" x="2256" y="1184" name="L_P2(15:0)" orien="R0" />
    </sheet>
</drawing>