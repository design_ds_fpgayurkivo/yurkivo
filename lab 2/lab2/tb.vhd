-- Vhdl test bench created from schematic C:\Plis\lab2\sh.sch - Mon Dec 18 19:31:00 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY sh_sh_sch_tb IS
END sh_sh_sch_tb;
ARCHITECTURE behavioral OF sh_sh_sch_tb IS 

   COMPONENT sh
   PORT( L_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          L_B	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          L_P	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          L_P2	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          L_P1	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

   SIGNAL L_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL L_B	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL L_P	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL L_P2	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL L_P1	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
	constant clk_c : time :=10 ns;
	SIGNAL buf: integer := 0;

BEGIN

   UUT: sh PORT MAP(
		L_A => L_A, 
		L_B => L_B, 
		L_P => L_P, 
		L_P2 => L_P2, 
		L_P1 => L_P1
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	l_A <="00000001";
	l_B <="00000010";
	buf <= buf+1;
	wait for clk_c;
	l_A <="00001111";
	l_B <="00000111";
   buf <= buf+1;
	wait for clk_c;	
	l_A <="11111111";
	l_B <="11111111";
	--l_A <= std_logic_vector( to_unsigned(buf,l_A'length));
	wait for clk_c;
     -- wait for clk_c/2;
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
