-- Vhdl test bench created from schematic C:\Plis\L_5\sh1.sch - Wed Dec 20 02:31:06 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY sh1_sh1_sch_tb IS
END sh1_sh1_sch_tb;
ARCHITECTURE behavioral OF sh1_sh1_sch_tb IS 

   COMPONENT sh1
   PORT( L_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          ce	:	IN	STD_LOGIC; 
          clk	:	IN	STD_LOGIC; 
          clr	:	IN	STD_LOGIC; 
          fir	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          rfd	:	OUT	STD_LOGIC; 
          rdy	:	OUT	STD_LOGIC; 
          ipfir	:	OUT	STD_LOGIC_VECTOR (17 DOWNTO 0));
   END COMPONENT;

   SIGNAL L_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0):=x"00";
   SIGNAL ce	:	STD_LOGIC:='1';
   SIGNAL clk	:	STD_LOGIC:='0';
   SIGNAL clr	:	STD_LOGIC:='0';
   SIGNAL fir	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL rfd	:	STD_LOGIC;
   SIGNAL rdy	:	STD_LOGIC;
   SIGNAL ipfir	:	STD_LOGIC_VECTOR (17 DOWNTO 0);

BEGIN

   UUT: sh1 PORT MAP(
		L_A => L_A, 
		ce => ce, 
		clk => clk, 
		clr => clr, 
		fir => fir, 
		rfd => rfd, 
		rdy => rdy, 
		ipfir => ipfir
   );

clk_process : PROCESS
	BEGIN
	CLK <='0';
	WAIT FOR 10 ns;
	CLK <= '1';
	WAIT FOR 10 ns;
	END PROCESS;
	data_change_process : PROCESS
	VARIABLE I : INTEGER;
	BEGIN
	for i in 0 to 3 loop 
	WAIT FOR 20 ns; 
	L_A <= conv_std_logic_vector(conv_integer(l_A) + 1,8); 
	end loop; 

	l_a<="00000000"; 
	for i in 0 to 4 loop 
	wait for 20 ns; 
	end loop; 

	for i in 0 to 3 loop 
	WAIT FOR 20 ns; 
	L_A <= conv_std_logic_vector(conv_integer(l_A) + 2,8); 
	end loop; 

	l_a<="00000000"; 
	for i in 0 to 4 loop 
	wait for 20 ns; 
	end loop; 
	END PROCESS;
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;

END;