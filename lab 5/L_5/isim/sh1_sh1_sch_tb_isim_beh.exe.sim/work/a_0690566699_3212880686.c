/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Plis/L_5/tb.vhd";
extern char *IEEE_P_3499444699;
extern char *IEEE_P_3620187407;

char *ieee_p_3499444699_sub_2213602152_3536714472(char *, char *, int , int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_0690566699_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;

LAB0:    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(61, ng0);
    t2 = (t0 + 4312);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(62, ng0);
    t7 = (10 * 1000LL);
    t2 = (t0 + 3240);
    xsi_process_wait(t2, t7);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(63, ng0);
    t2 = (t0 + 4312);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(64, ng0);
    t7 = (10 * 1000LL);
    t2 = (t0 + 3240);
    xsi_process_wait(t2, t7);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_0690566699_3212880686_p_1(char *t0)
{
    char t8[16];
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    int64 t6;
    char *t7;
    int t9;
    int t10;
    char *t11;
    unsigned int t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    t1 = (t0 + 3680U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(69, ng0);
    t2 = (t0 + 6364);
    *((int *)t2) = 0;
    t3 = (t0 + 6368);
    *((int *)t3) = 3;
    t4 = 0;
    t5 = 3;

LAB4:    if (t4 <= t5)
        goto LAB5;

LAB7:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 6372);
    t7 = (t0 + 4376);
    t11 = (t7 + 56U);
    t14 = *((char **)t11);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 8U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(75, ng0);
    t2 = (t0 + 6380);
    *((int *)t2) = 0;
    t3 = (t0 + 6384);
    *((int *)t3) = 4;
    t4 = 0;
    t5 = 4;

LAB15:    if (t4 <= t5)
        goto LAB16;

LAB18:    xsi_set_current_line(79, ng0);
    t2 = (t0 + 6388);
    *((int *)t2) = 0;
    t3 = (t0 + 6392);
    *((int *)t3) = 3;
    t4 = 0;
    t5 = 3;

LAB24:    if (t4 <= t5)
        goto LAB25;

LAB27:    xsi_set_current_line(84, ng0);
    t2 = (t0 + 6396);
    t7 = (t0 + 4376);
    t11 = (t7 + 56U);
    t14 = *((char **)t11);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t2, 8U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(85, ng0);
    t2 = (t0 + 6404);
    *((int *)t2) = 0;
    t3 = (t0 + 6408);
    *((int *)t3) = 4;
    t4 = 0;
    t5 = 4;

LAB35:    if (t4 <= t5)
        goto LAB36;

LAB38:    goto LAB2;

LAB5:    xsi_set_current_line(70, ng0);
    t6 = (20 * 1000LL);
    t7 = (t0 + 3488);
    xsi_process_wait(t7, t6);

LAB10:    *((char **)t1) = &&LAB11;

LAB1:    return;
LAB6:    t2 = (t0 + 6364);
    t4 = *((int *)t2);
    t3 = (t0 + 6368);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB7;

LAB14:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 6364);
    *((int *)t7) = t4;
    goto LAB4;

LAB8:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 6256U);
    t9 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t2);
    t10 = (t9 + 1);
    t7 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t8, t10, 8);
    t11 = (t8 + 12U);
    t12 = *((unsigned int *)t11);
    t12 = (t12 * 1U);
    t13 = (8U != t12);
    if (t13 == 1)
        goto LAB12;

LAB13:    t14 = (t0 + 4376);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    memcpy(t18, t7, 8U);
    xsi_driver_first_trans_fast(t14);
    goto LAB6;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    xsi_size_not_matching(8U, t12, 0);
    goto LAB13;

LAB16:    xsi_set_current_line(76, ng0);
    t6 = (20 * 1000LL);
    t7 = (t0 + 3488);
    xsi_process_wait(t7, t6);

LAB21:    *((char **)t1) = &&LAB22;
    goto LAB1;

LAB17:    t2 = (t0 + 6380);
    t4 = *((int *)t2);
    t3 = (t0 + 6384);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB18;

LAB23:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 6380);
    *((int *)t7) = t4;
    goto LAB15;

LAB19:    goto LAB17;

LAB20:    goto LAB19;

LAB22:    goto LAB20;

LAB25:    xsi_set_current_line(80, ng0);
    t6 = (20 * 1000LL);
    t7 = (t0 + 3488);
    xsi_process_wait(t7, t6);

LAB30:    *((char **)t1) = &&LAB31;
    goto LAB1;

LAB26:    t2 = (t0 + 6388);
    t4 = *((int *)t2);
    t3 = (t0 + 6392);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB27;

LAB34:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 6388);
    *((int *)t7) = t4;
    goto LAB24;

LAB28:    xsi_set_current_line(81, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t2 = (t0 + 6256U);
    t9 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t2);
    t10 = (t9 + 2);
    t7 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t8, t10, 8);
    t11 = (t8 + 12U);
    t12 = *((unsigned int *)t11);
    t12 = (t12 * 1U);
    t13 = (8U != t12);
    if (t13 == 1)
        goto LAB32;

LAB33:    t14 = (t0 + 4376);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    memcpy(t18, t7, 8U);
    xsi_driver_first_trans_fast(t14);
    goto LAB26;

LAB29:    goto LAB28;

LAB31:    goto LAB29;

LAB32:    xsi_size_not_matching(8U, t12, 0);
    goto LAB33;

LAB36:    xsi_set_current_line(86, ng0);
    t6 = (20 * 1000LL);
    t7 = (t0 + 3488);
    xsi_process_wait(t7, t6);

LAB41:    *((char **)t1) = &&LAB42;
    goto LAB1;

LAB37:    t2 = (t0 + 6404);
    t4 = *((int *)t2);
    t3 = (t0 + 6408);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB38;

LAB43:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 6404);
    *((int *)t7) = t4;
    goto LAB35;

LAB39:    goto LAB37;

LAB40:    goto LAB39;

LAB42:    goto LAB40;

}

static void work_a_0690566699_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;

LAB0:    t1 = (t0 + 3928U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(91, ng0);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

}


extern void work_a_0690566699_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0690566699_3212880686_p_0,(void *)work_a_0690566699_3212880686_p_1,(void *)work_a_0690566699_3212880686_p_2};
	xsi_register_didat("work_a_0690566699_3212880686", "isim/sh1_sh1_sch_tb_isim_beh.exe.sim/work/a_0690566699_3212880686.didat");
	xsi_register_executes(pe);
}
