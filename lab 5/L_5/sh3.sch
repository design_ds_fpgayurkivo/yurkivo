<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="L_A(7:0)" />
        <signal name="fir(15:0)" />
        <signal name="clk" />
        <signal name="clr" />
        <signal name="ce" />
        <signal name="rfd" />
        <signal name="rdy" />
        <signal name="ipfir(16:0)" />
        <port polarity="Input" name="L_A(7:0)" />
        <port polarity="Output" name="fir(15:0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="clr" />
        <port polarity="Input" name="ce" />
        <port polarity="Output" name="rfd" />
        <port polarity="Output" name="rdy" />
        <port polarity="Output" name="ipfir(16:0)" />
        <blockdef name="sh">
            <timestamp>2017-12-26T20:14:55</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="ip">
            <timestamp>2017-12-26T20:14:38</timestamp>
            <rect width="512" x="32" y="32" height="2016" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="1008" y2="1008" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
            <line x2="544" y1="1840" y2="1840" x1="576" />
            <line x2="544" y1="1872" y2="1872" x1="576" />
        </blockdef>
        <block symbolname="sh" name="XLXI_1">
            <blockpin signalname="L_A(7:0)" name="L_A(7:0)" />
            <blockpin signalname="clr" name="clr" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="ce" name="ce" />
            <blockpin signalname="fir(15:0)" name="F(15:0)" />
        </block>
        <block symbolname="ip" name="XLXI_2">
            <blockpin signalname="L_A(7:0)" name="din(7:0)" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="ipfir(16:0)" name="dout(16:0)" />
            <blockpin signalname="rfd" name="rfd" />
            <blockpin signalname="rdy" name="rdy" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="992" y="1808" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1696" y="192" name="XLXI_2" orien="R0">
        </instance>
        <branch name="L_A(7:0)">
            <wire x2="832" y1="272" y2="272" x1="416" />
            <wire x2="1696" y1="272" y2="272" x1="832" />
            <wire x2="832" y1="272" y2="1584" x1="832" />
            <wire x2="992" y1="1584" y2="1584" x1="832" />
        </branch>
        <branch name="fir(15:0)">
            <wire x2="1472" y1="1584" y2="1584" x1="1376" />
            <wire x2="1472" y1="1584" y2="1872" x1="1472" />
        </branch>
        <branch name="clk">
            <wire x2="720" y1="1200" y2="1200" x1="496" />
            <wire x2="1696" y1="1200" y2="1200" x1="720" />
            <wire x2="720" y1="1200" y2="1776" x1="720" />
            <wire x2="992" y1="1776" y2="1776" x1="720" />
        </branch>
        <branch name="clr">
            <wire x2="544" y1="1632" y2="1648" x1="544" />
            <wire x2="992" y1="1648" y2="1648" x1="544" />
        </branch>
        <branch name="ce">
            <wire x2="992" y1="1712" y2="1712" x1="544" />
        </branch>
        <branch name="rfd">
            <wire x2="2448" y1="2032" y2="2032" x1="2272" />
        </branch>
        <branch name="rdy">
            <wire x2="2544" y1="2064" y2="2064" x1="2272" />
        </branch>
        <iomarker fontsize="28" x="2448" y="2032" name="rfd" orien="R0" />
        <iomarker fontsize="28" x="2544" y="2064" name="rdy" orien="R0" />
        <iomarker fontsize="28" x="1472" y="1872" name="fir(15:0)" orien="R90" />
        <iomarker fontsize="28" x="544" y="1712" name="ce" orien="R180" />
        <iomarker fontsize="28" x="544" y="1632" name="clr" orien="R270" />
        <iomarker fontsize="28" x="496" y="1200" name="clk" orien="R180" />
        <iomarker fontsize="28" x="416" y="272" name="L_A(7:0)" orien="R180" />
        <branch name="ipfir(16:0)">
            <wire x2="2368" y1="272" y2="272" x1="2272" />
        </branch>
        <iomarker fontsize="28" x="2368" y="272" name="ipfir(16:0)" orien="R0" />
    </sheet>
</drawing>