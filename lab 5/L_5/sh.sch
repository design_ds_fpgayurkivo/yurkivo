<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(7:0)" />
        <signal name="XLXN_2(7:0)" />
        <signal name="XLXN_3(7:0)" />
        <signal name="L_A(7:0)" />
        <signal name="clr" />
        <signal name="clk" />
        <signal name="F(15:0)" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_36(15:0)" />
        <signal name="XLXN_37(15:0)" />
        <signal name="XLXN_38(15:0)" />
        <signal name="XLXN_39(15:0)" />
        <signal name="XLXN_40(15:0)" />
        <signal name="XLXN_41(15:0)" />
        <signal name="XLXN_42(15:0)" />
        <signal name="XLXN_43(15:0)" />
        <signal name="XLXN_44(15:0)" />
        <signal name="ce" />
        <signal name="XLXN_50(7:0)" />
        <signal name="XLXN_51(15:0)" />
        <port polarity="Input" name="L_A(7:0)" />
        <port polarity="Input" name="clr" />
        <port polarity="Input" name="clk" />
        <port polarity="Output" name="F(15:0)" />
        <port polarity="Input" name="ce" />
        <blockdef name="m">
            <timestamp>2017-12-26T20:5:23</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="mM">
            <timestamp>2017-12-26T20:5:15</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="fd8ce" name="XLXI_8">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="L_A(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_1(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_9">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_1(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_2(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_10">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_2(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_3(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_11">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_3(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_50(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="add16" name="XLXI_14">
            <blockpin signalname="XLXN_36(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_37(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_30" name="CI" />
            <blockpin signalname="XLXN_31" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_38(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_15">
            <blockpin signalname="XLXN_39(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_38(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_31" name="CI" />
            <blockpin signalname="XLXN_32" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_40(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_16">
            <blockpin signalname="XLXN_41(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_40(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_32" name="CI" />
            <blockpin signalname="XLXN_33" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_42(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_17">
            <blockpin signalname="XLXN_43(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_42(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_33" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_44(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_20">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="ce" name="CE" />
            <blockpin signalname="clr" name="CLR" />
            <blockpin signalname="XLXN_44(15:0)" name="D(15:0)" />
            <blockpin signalname="F(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="m" name="XLXI_1">
            <blockpin signalname="L_A(7:0)" name="l_A(7:0)" />
            <blockpin signalname="XLXN_36(15:0)" name="l_out2(15:0)" />
        </block>
        <block symbolname="mM" name="XLXI_3">
            <blockpin signalname="XLXN_2(7:0)" name="L_A(7:0)" />
            <blockpin signalname="XLXN_39(15:0)" name="L_out1(15:0)" />
        </block>
        <block symbolname="mM" name="XLXI_4">
            <blockpin signalname="XLXN_3(7:0)" name="L_A(7:0)" />
            <blockpin signalname="XLXN_41(15:0)" name="L_out1(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_28">
            <blockpin signalname="XLXN_30" name="G" />
        </block>
        <block symbolname="m" name="XLXI_29">
            <blockpin signalname="XLXN_1(7:0)" name="l_A(7:0)" />
            <blockpin signalname="XLXN_37(15:0)" name="l_out2(15:0)" />
        </block>
        <block symbolname="mM" name="XLXI_30">
            <blockpin signalname="XLXN_50(7:0)" name="L_A(7:0)" />
            <blockpin signalname="XLXN_43(15:0)" name="L_out1(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="1312" y="2128" name="XLXI_1" orien="R0">
        </instance>
        <instance x="2688" y="2112" name="XLXI_3" orien="R0">
        </instance>
        <instance x="3440" y="2128" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1072" y="1360" name="XLXI_8" orien="R0" />
        <instance x="1712" y="1360" name="XLXI_9" orien="R0" />
        <instance x="2336" y="1360" name="XLXI_10" orien="R0" />
        <instance x="2992" y="1360" name="XLXI_11" orien="R0" />
        <instance x="1712" y="3136" name="XLXI_14" orien="R0" />
        <instance x="2592" y="3152" name="XLXI_15" orien="R0" />
        <instance x="3424" y="3120" name="XLXI_16" orien="R0" />
        <instance x="4496" y="3040" name="XLXI_17" orien="R0" />
        <instance x="5856" y="3776" name="XLXI_20" orien="R0" />
        <branch name="XLXN_1(7:0)">
            <wire x2="1600" y1="1104" y2="1104" x1="1456" />
            <wire x2="1712" y1="1104" y2="1104" x1="1600" />
            <wire x2="1600" y1="1104" y2="1392" x1="1600" />
            <wire x2="2000" y1="1392" y2="1392" x1="1600" />
            <wire x2="2000" y1="1392" y2="2080" x1="2000" />
        </branch>
        <branch name="XLXN_2(7:0)">
            <wire x2="2208" y1="1104" y2="1104" x1="2096" />
            <wire x2="2336" y1="1104" y2="1104" x1="2208" />
            <wire x2="2208" y1="1104" y2="1408" x1="2208" />
            <wire x2="2592" y1="1408" y2="1408" x1="2208" />
            <wire x2="2592" y1="1408" y2="2080" x1="2592" />
            <wire x2="2688" y1="2080" y2="2080" x1="2592" />
        </branch>
        <branch name="XLXN_3(7:0)">
            <wire x2="2864" y1="1104" y2="1104" x1="2720" />
            <wire x2="2992" y1="1104" y2="1104" x1="2864" />
            <wire x2="2864" y1="1104" y2="1504" x1="2864" />
            <wire x2="3424" y1="1504" y2="1504" x1="2864" />
            <wire x2="3424" y1="1504" y2="2096" x1="3424" />
            <wire x2="3440" y1="2096" y2="2096" x1="3424" />
        </branch>
        <branch name="L_A(7:0)">
            <wire x2="976" y1="1104" y2="1104" x1="928" />
            <wire x2="1072" y1="1104" y2="1104" x1="976" />
            <wire x2="976" y1="1104" y2="2096" x1="976" />
            <wire x2="1312" y1="2096" y2="2096" x1="976" />
        </branch>
        <iomarker fontsize="28" x="928" y="1104" name="L_A(7:0)" orien="R180" />
        <branch name="clr">
            <wire x2="928" y1="1328" y2="1328" x1="880" />
            <wire x2="1072" y1="1328" y2="1328" x1="928" />
            <wire x2="928" y1="1328" y2="1520" x1="928" />
            <wire x2="928" y1="1520" y2="3744" x1="928" />
            <wire x2="5856" y1="3744" y2="3744" x1="928" />
            <wire x2="1712" y1="1520" y2="1520" x1="928" />
            <wire x2="2336" y1="1520" y2="1520" x1="1712" />
            <wire x2="2992" y1="1520" y2="1520" x1="2336" />
            <wire x2="1712" y1="1328" y2="1520" x1="1712" />
            <wire x2="2336" y1="1328" y2="1520" x1="2336" />
            <wire x2="2992" y1="1328" y2="1520" x1="2992" />
        </branch>
        <iomarker fontsize="28" x="880" y="1328" name="clr" orien="R180" />
        <branch name="clk">
            <wire x2="784" y1="1232" y2="1232" x1="656" />
            <wire x2="1072" y1="1232" y2="1232" x1="784" />
            <wire x2="784" y1="1232" y2="1600" x1="784" />
            <wire x2="784" y1="1600" y2="3616" x1="784" />
            <wire x2="2096" y1="3616" y2="3616" x1="784" />
            <wire x2="2096" y1="3616" y2="3648" x1="2096" />
            <wire x2="5856" y1="3648" y2="3648" x1="2096" />
            <wire x2="1504" y1="1600" y2="1600" x1="784" />
            <wire x2="2160" y1="1600" y2="1600" x1="1504" />
            <wire x2="2768" y1="1600" y2="1600" x1="2160" />
            <wire x2="1504" y1="1232" y2="1600" x1="1504" />
            <wire x2="1712" y1="1232" y2="1232" x1="1504" />
            <wire x2="2160" y1="1232" y2="1600" x1="2160" />
            <wire x2="2336" y1="1232" y2="1232" x1="2160" />
            <wire x2="2768" y1="1232" y2="1600" x1="2768" />
            <wire x2="2992" y1="1232" y2="1232" x1="2768" />
        </branch>
        <iomarker fontsize="28" x="656" y="1232" name="clk" orien="R180" />
        <branch name="F(15:0)">
            <wire x2="6400" y1="3520" y2="3520" x1="6240" />
        </branch>
        <iomarker fontsize="28" x="6400" y="3520" name="F(15:0)" orien="R0" />
        <instance x="1120" y="2880" name="XLXI_28" orien="R0" />
        <branch name="XLXN_30">
            <wire x2="1712" y1="2688" y2="2688" x1="1184" />
            <wire x2="1184" y1="2688" y2="2752" x1="1184" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="2368" y1="3072" y2="3072" x1="2160" />
            <wire x2="2368" y1="2704" y2="3072" x1="2368" />
            <wire x2="2592" y1="2704" y2="2704" x1="2368" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="3232" y1="3088" y2="3088" x1="3040" />
            <wire x2="3232" y1="2672" y2="3088" x1="3232" />
            <wire x2="3424" y1="2672" y2="2672" x1="3232" />
        </branch>
        <branch name="XLXN_33">
            <wire x2="4176" y1="3056" y2="3056" x1="3872" />
            <wire x2="4176" y1="2592" y2="3056" x1="4176" />
            <wire x2="4496" y1="2592" y2="2592" x1="4176" />
        </branch>
        <branch name="XLXN_36(15:0)">
            <wire x2="1760" y1="2192" y2="2192" x1="1648" />
            <wire x2="1648" y1="2192" y2="2816" x1="1648" />
            <wire x2="1712" y1="2816" y2="2816" x1="1648" />
            <wire x2="1760" y1="2096" y2="2096" x1="1696" />
            <wire x2="1760" y1="2096" y2="2192" x1="1760" />
        </branch>
        <branch name="XLXN_37(15:0)">
            <wire x2="1664" y1="2608" y2="2944" x1="1664" />
            <wire x2="1712" y1="2944" y2="2944" x1="1664" />
            <wire x2="2464" y1="2608" y2="2608" x1="1664" />
            <wire x2="2464" y1="2080" y2="2080" x1="2384" />
            <wire x2="2464" y1="2080" y2="2608" x1="2464" />
        </branch>
        <branch name="XLXN_38(15:0)">
            <wire x2="2352" y1="2880" y2="2880" x1="2160" />
            <wire x2="2352" y1="2880" y2="2960" x1="2352" />
            <wire x2="2592" y1="2960" y2="2960" x1="2352" />
        </branch>
        <branch name="XLXN_39(15:0)">
            <wire x2="3152" y1="2624" y2="2624" x1="2528" />
            <wire x2="2528" y1="2624" y2="2832" x1="2528" />
            <wire x2="2592" y1="2832" y2="2832" x1="2528" />
            <wire x2="3152" y1="2080" y2="2080" x1="3072" />
            <wire x2="3152" y1="2080" y2="2624" x1="3152" />
        </branch>
        <branch name="XLXN_40(15:0)">
            <wire x2="3216" y1="2896" y2="2896" x1="3040" />
            <wire x2="3216" y1="2896" y2="2928" x1="3216" />
            <wire x2="3424" y1="2928" y2="2928" x1="3216" />
        </branch>
        <branch name="XLXN_41(15:0)">
            <wire x2="3920" y1="2464" y2="2464" x1="3344" />
            <wire x2="3344" y1="2464" y2="2800" x1="3344" />
            <wire x2="3424" y1="2800" y2="2800" x1="3344" />
            <wire x2="3920" y1="2096" y2="2096" x1="3824" />
            <wire x2="3920" y1="2096" y2="2464" x1="3920" />
        </branch>
        <branch name="XLXN_42(15:0)">
            <wire x2="4160" y1="2864" y2="2864" x1="3872" />
            <wire x2="4160" y1="2848" y2="2864" x1="4160" />
            <wire x2="4496" y1="2848" y2="2848" x1="4160" />
        </branch>
        <branch name="XLXN_43(15:0)">
            <wire x2="4416" y1="2512" y2="2720" x1="4416" />
            <wire x2="4496" y1="2720" y2="2720" x1="4416" />
            <wire x2="4656" y1="2512" y2="2512" x1="4416" />
            <wire x2="4656" y1="2080" y2="2080" x1="4576" />
            <wire x2="4656" y1="2080" y2="2512" x1="4656" />
        </branch>
        <branch name="XLXN_44(15:0)">
            <wire x2="5184" y1="2784" y2="2784" x1="4944" />
            <wire x2="5184" y1="2784" y2="2832" x1="5184" />
            <wire x2="5456" y1="2832" y2="2832" x1="5184" />
            <wire x2="5456" y1="2832" y2="3520" x1="5456" />
            <wire x2="5856" y1="3520" y2="3520" x1="5456" />
        </branch>
        <branch name="ce">
            <wire x2="736" y1="1168" y2="1168" x1="496" />
            <wire x2="1072" y1="1168" y2="1168" x1="736" />
            <wire x2="736" y1="1168" y2="1440" x1="736" />
            <wire x2="736" y1="1440" y2="2336" x1="736" />
            <wire x2="1520" y1="1440" y2="1440" x1="736" />
            <wire x2="2176" y1="1440" y2="1440" x1="1520" />
            <wire x2="2784" y1="1440" y2="1440" x1="2176" />
            <wire x2="608" y1="2336" y2="3520" x1="608" />
            <wire x2="1680" y1="3520" y2="3520" x1="608" />
            <wire x2="3616" y1="3520" y2="3520" x1="1680" />
            <wire x2="3616" y1="3520" y2="3584" x1="3616" />
            <wire x2="5856" y1="3584" y2="3584" x1="3616" />
            <wire x2="736" y1="2336" y2="2336" x1="608" />
            <wire x2="1520" y1="1168" y2="1440" x1="1520" />
            <wire x2="1712" y1="1168" y2="1168" x1="1520" />
            <wire x2="2176" y1="1168" y2="1440" x1="2176" />
            <wire x2="2336" y1="1168" y2="1168" x1="2176" />
            <wire x2="2784" y1="1168" y2="1440" x1="2784" />
            <wire x2="2992" y1="1168" y2="1168" x1="2784" />
        </branch>
        <iomarker fontsize="28" x="496" y="1168" name="ce" orien="R180" />
        <instance x="2000" y="2112" name="XLXI_29" orien="R0">
        </instance>
        <instance x="4192" y="2112" name="XLXI_30" orien="R0">
        </instance>
        <branch name="XLXN_50(7:0)">
            <wire x2="3840" y1="1104" y2="1104" x1="3376" />
            <wire x2="3840" y1="1104" y2="2080" x1="3840" />
            <wire x2="4192" y1="2080" y2="2080" x1="3840" />
        </branch>
    </sheet>
</drawing>