////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: l_ip.v
// /___/   /\     Timestamp: Tue Dec 26 21:09:20 2017
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog C:/Plis/l_3/ipcore_dir/tmp/_cg/l_ip.ngc C:/Plis/l_3/ipcore_dir/tmp/_cg/l_ip.v 
// Device	: 4vfx12sf363-12
// Input file	: C:/Plis/l_3/ipcore_dir/tmp/_cg/l_ip.ngc
// Output file	: C:/Plis/l_3/ipcore_dir/tmp/_cg/l_ip.v
// # of Modules	: 1
// Design Name	: l_ip
// Xilinx        : D:\Xilinx\14.7\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module l_ip (
p, a
)/* synthesis syn_black_box syn_noprune=1 */;
  output [36 : 0] p;
  input [7 : 0] a;
  
  // synthesis translate_off
  
  wire \blk00000001/sig0000006b ;
  wire \blk00000001/sig0000006a ;
  wire \blk00000001/sig00000069 ;
  wire \blk00000001/sig00000068 ;
  wire \blk00000001/sig00000067 ;
  wire \blk00000001/sig00000066 ;
  wire \blk00000001/sig00000065 ;
  wire \blk00000001/sig00000064 ;
  wire \blk00000001/sig00000063 ;
  wire \blk00000001/sig00000062 ;
  wire \blk00000001/sig00000061 ;
  wire \blk00000001/sig00000060 ;
  wire \blk00000001/sig0000005f ;
  wire \blk00000001/sig0000005e ;
  wire \blk00000001/sig0000005d ;
  wire \blk00000001/sig0000005c ;
  wire \blk00000001/sig0000005b ;
  wire \blk00000001/sig0000005a ;
  wire \blk00000001/sig00000059 ;
  wire \blk00000001/sig00000058 ;
  wire \blk00000001/sig00000057 ;
  wire \blk00000001/sig00000056 ;
  wire \blk00000001/sig00000055 ;
  wire \blk00000001/sig00000054 ;
  wire \blk00000001/sig00000053 ;
  wire \blk00000001/sig00000052 ;
  wire \blk00000001/sig00000051 ;
  wire \blk00000001/sig00000050 ;
  wire \blk00000001/sig0000004f ;
  wire \blk00000001/sig0000004e ;
  wire \blk00000001/sig0000004d ;
  wire \blk00000001/sig0000004c ;
  wire \blk00000001/sig0000004b ;
  wire \blk00000001/sig0000004a ;
  wire \blk00000001/sig00000049 ;
  wire \blk00000001/sig00000048 ;
  wire \blk00000001/sig00000047 ;
  wire \blk00000001/sig00000046 ;
  wire \blk00000001/sig00000045 ;
  wire \blk00000001/sig00000044 ;
  wire \blk00000001/sig00000043 ;
  wire \blk00000001/sig00000042 ;
  wire \blk00000001/sig00000041 ;
  wire \blk00000001/sig00000040 ;
  wire \blk00000001/sig0000003f ;
  wire \blk00000001/sig0000003e ;
  wire \blk00000001/sig0000003d ;
  wire \blk00000001/sig0000003c ;
  wire \blk00000001/sig0000003b ;
  wire \blk00000001/sig0000003a ;
  wire \blk00000001/sig00000039 ;
  wire \blk00000001/sig00000038 ;
  wire \blk00000001/sig00000037 ;
  wire \blk00000001/sig00000036 ;
  wire \blk00000001/sig00000035 ;
  wire \blk00000001/sig00000034 ;
  wire \blk00000001/sig00000033 ;
  wire \blk00000001/sig00000032 ;
  wire \blk00000001/sig00000031 ;
  wire \blk00000001/sig00000030 ;
  wire \blk00000001/sig0000002f ;
  wire \blk00000001/sig0000002e ;
  wire \blk00000001/sig0000002d ;
  wire \blk00000001/sig0000002c ;
  wire \blk00000001/sig0000002b ;
  wire \blk00000001/sig0000002a ;
  wire \blk00000001/sig00000029 ;
  wire \blk00000001/sig00000028 ;
  wire \blk00000001/sig00000027 ;
  wire \blk00000001/sig00000026 ;
  wire \blk00000001/sig00000025 ;
  wire \blk00000001/sig00000024 ;
  wire \blk00000001/sig00000023 ;
  wire \blk00000001/sig00000022 ;
  wire \blk00000001/sig00000021 ;
  wire \blk00000001/sig00000020 ;
  wire \blk00000001/sig0000001f ;
  wire \blk00000001/sig0000001e ;
  wire \blk00000001/sig0000001d ;
  wire \blk00000001/sig0000001c ;
  wire \blk00000001/sig0000001b ;
  wire \blk00000001/sig0000001a ;
  wire \blk00000001/sig00000019 ;
  wire \blk00000001/sig00000018 ;
  wire \blk00000001/sig00000017 ;
  wire \blk00000001/sig00000016 ;
  wire \blk00000001/sig00000015 ;
  wire \blk00000001/sig00000014 ;
  wire \blk00000001/sig00000013 ;
  wire \blk00000001/sig00000012 ;
  wire \blk00000001/sig00000011 ;
  wire \blk00000001/sig00000010 ;
  wire \blk00000001/sig0000000f ;
  wire \blk00000001/sig0000000e ;
  wire \blk00000001/sig0000000d ;
  wire \blk00000001/sig0000000c ;
  wire \blk00000001/sig0000000a ;
  wire \blk00000001/sig00000009 ;
  wire [1 : 0] NlwRenamedSignal_a;
  wire [36 : 36] NlwRenamedSig_OI_p;
  assign
    p[36] = NlwRenamedSig_OI_p[36],
    p[1] = NlwRenamedSignal_a[1],
    p[0] = NlwRenamedSignal_a[0],
    NlwRenamedSignal_a[1] = a[1],
    NlwRenamedSignal_a[0] = a[0];
  LUT4 #(
    .INIT ( 16'h9D62 ))
  \blk00000001/blk00000086  (
    .I0(a[3]),
    .I1(a[2]),
    .I2(NlwRenamedSignal_a[1]),
    .I3(\blk00000001/sig0000000a ),
    .O(\blk00000001/sig0000003b )
  );
  LUT4 #(
    .INIT ( 16'h1998 ))
  \blk00000001/blk00000085  (
    .I0(a[4]),
    .I1(a[6]),
    .I2(a[5]),
    .I3(a[7]),
    .O(\blk00000001/sig0000000a )
  );
  LUT3 #(
    .INIT ( 8'h6C ))
  \blk00000001/blk00000084  (
    .I0(a[2]),
    .I1(\blk00000001/sig00000009 ),
    .I2(a[3]),
    .O(\blk00000001/sig0000003c )
  );
  LUT4 #(
    .INIT ( 16'h6998 ))
  \blk00000001/blk00000083  (
    .I0(a[5]),
    .I1(a[7]),
    .I2(a[4]),
    .I3(a[6]),
    .O(\blk00000001/sig00000009 )
  );
  LUT3 #(
    .INIT ( 8'h96 ))
  \blk00000001/blk00000082  (
    .I0(\blk00000001/sig00000067 ),
    .I1(a[6]),
    .I2(a[4]),
    .O(\blk00000001/sig00000042 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000081  (
    .I0(a[7]),
    .O(\blk00000001/sig0000004a )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000080  (
    .I0(NlwRenamedSignal_a[0]),
    .O(\blk00000001/sig0000001d )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000007f  (
    .I0(NlwRenamedSignal_a[1]),
    .O(\blk00000001/sig0000001f )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000007e  (
    .I0(a[2]),
    .O(\blk00000001/sig00000021 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000007d  (
    .I0(a[3]),
    .O(\blk00000001/sig00000023 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000007c  (
    .I0(a[4]),
    .O(\blk00000001/sig00000025 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000007b  (
    .I0(a[5]),
    .O(\blk00000001/sig00000027 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000007a  (
    .I0(a[6]),
    .O(\blk00000001/sig0000002a )
  );
  LUT4 #(
    .INIT ( 16'hE7A6 ))
  \blk00000001/blk00000079  (
    .I0(a[2]),
    .I1(NlwRenamedSignal_a[1]),
    .I2(a[3]),
    .I3(NlwRenamedSignal_a[0]),
    .O(\blk00000001/sig00000065 )
  );
  LUT4 #(
    .INIT ( 16'h9B92 ))
  \blk00000001/blk00000078  (
    .I0(a[3]),
    .I1(NlwRenamedSignal_a[0]),
    .I2(a[2]),
    .I3(NlwRenamedSignal_a[1]),
    .O(\blk00000001/sig00000066 )
  );
  LUT4 #(
    .INIT ( 16'h93B2 ))
  \blk00000001/blk00000077  (
    .I0(NlwRenamedSignal_a[0]),
    .I1(NlwRenamedSignal_a[1]),
    .I2(a[3]),
    .I3(a[2]),
    .O(\blk00000001/sig00000067 )
  );
  LUT4 #(
    .INIT ( 16'h5998 ))
  \blk00000001/blk00000076  (
    .I0(a[2]),
    .I1(NlwRenamedSignal_a[0]),
    .I2(NlwRenamedSignal_a[1]),
    .I3(a[3]),
    .O(\blk00000001/sig00000068 )
  );
  LUT4 #(
    .INIT ( 16'h6918 ))
  \blk00000001/blk00000075  (
    .I0(NlwRenamedSignal_a[1]),
    .I1(a[3]),
    .I2(NlwRenamedSignal_a[0]),
    .I3(a[2]),
    .O(\blk00000001/sig00000069 )
  );
  LUT4 #(
    .INIT ( 16'h6696 ))
  \blk00000001/blk00000074  (
    .I0(a[5]),
    .I1(a[7]),
    .I2(a[4]),
    .I3(a[6]),
    .O(\blk00000001/sig00000051 )
  );
  LUT4 #(
    .INIT ( 16'hE786 ))
  \blk00000001/blk00000073  (
    .I0(NlwRenamedSignal_a[1]),
    .I1(a[3]),
    .I2(a[2]),
    .I3(NlwRenamedSignal_a[0]),
    .O(\blk00000001/sig0000005b )
  );
  LUT4 #(
    .INIT ( 16'hE7A6 ))
  \blk00000001/blk00000072  (
    .I0(a[6]),
    .I1(a[5]),
    .I2(a[7]),
    .I3(a[4]),
    .O(\blk00000001/sig00000052 )
  );
  LUT4 #(
    .INIT ( 16'h7918 ))
  \blk00000001/blk00000071  (
    .I0(a[3]),
    .I1(NlwRenamedSignal_a[0]),
    .I2(a[2]),
    .I3(NlwRenamedSignal_a[1]),
    .O(\blk00000001/sig0000006a )
  );
  LUT4 #(
    .INIT ( 16'h9B92 ))
  \blk00000001/blk00000070  (
    .I0(a[7]),
    .I1(a[4]),
    .I2(a[6]),
    .I3(a[5]),
    .O(\blk00000001/sig00000053 )
  );
  LUT4 #(
    .INIT ( 16'hE786 ))
  \blk00000001/blk0000006f  (
    .I0(NlwRenamedSignal_a[0]),
    .I1(a[3]),
    .I2(NlwRenamedSignal_a[1]),
    .I3(a[2]),
    .O(\blk00000001/sig0000006b )
  );
  LUT4 #(
    .INIT ( 16'h93B2 ))
  \blk00000001/blk0000006e  (
    .I0(a[4]),
    .I1(a[5]),
    .I2(a[7]),
    .I3(a[6]),
    .O(\blk00000001/sig00000054 )
  );
  LUT4 #(
    .INIT ( 16'h7918 ))
  \blk00000001/blk0000006d  (
    .I0(a[2]),
    .I1(NlwRenamedSignal_a[0]),
    .I2(NlwRenamedSignal_a[1]),
    .I3(a[3]),
    .O(\blk00000001/sig00000059 )
  );
  LUT4 #(
    .INIT ( 16'h5998 ))
  \blk00000001/blk0000006c  (
    .I0(a[6]),
    .I1(a[4]),
    .I2(a[5]),
    .I3(a[7]),
    .O(\blk00000001/sig00000055 )
  );
  LUT4 #(
    .INIT ( 16'h4924 ))
  \blk00000001/blk0000006b  (
    .I0(NlwRenamedSignal_a[1]),
    .I1(NlwRenamedSignal_a[0]),
    .I2(a[3]),
    .I3(a[2]),
    .O(\blk00000001/sig0000005a )
  );
  LUT4 #(
    .INIT ( 16'h6918 ))
  \blk00000001/blk0000006a  (
    .I0(a[5]),
    .I1(a[7]),
    .I2(a[4]),
    .I3(a[6]),
    .O(\blk00000001/sig00000056 )
  );
  LUT4 #(
    .INIT ( 16'hE786 ))
  \blk00000001/blk00000069  (
    .I0(a[5]),
    .I1(a[7]),
    .I2(a[6]),
    .I3(a[4]),
    .O(\blk00000001/sig0000004d )
  );
  LUT4 #(
    .INIT ( 16'h9DD4 ))
  \blk00000001/blk00000068  (
    .I0(a[3]),
    .I1(a[2]),
    .I2(NlwRenamedSignal_a[1]),
    .I3(NlwRenamedSignal_a[0]),
    .O(\blk00000001/sig0000005c )
  );
  LUT4 #(
    .INIT ( 16'h7918 ))
  \blk00000001/blk00000067  (
    .I0(a[7]),
    .I1(a[4]),
    .I2(a[6]),
    .I3(a[5]),
    .O(\blk00000001/sig00000057 )
  );
  LUT4 #(
    .INIT ( 16'h93B2 ))
  \blk00000001/blk00000066  (
    .I0(a[3]),
    .I1(NlwRenamedSignal_a[0]),
    .I2(a[2]),
    .I3(NlwRenamedSignal_a[1]),
    .O(\blk00000001/sig0000005d )
  );
  LUT4 #(
    .INIT ( 16'hE786 ))
  \blk00000001/blk00000065  (
    .I0(a[7]),
    .I1(a[4]),
    .I2(a[5]),
    .I3(a[6]),
    .O(\blk00000001/sig00000058 )
  );
  LUT4 #(
    .INIT ( 16'h9332 ))
  \blk00000001/blk00000064  (
    .I0(NlwRenamedSignal_a[0]),
    .I1(NlwRenamedSignal_a[1]),
    .I2(a[3]),
    .I3(a[2]),
    .O(\blk00000001/sig0000005e )
  );
  LUT4 #(
    .INIT ( 16'h7918 ))
  \blk00000001/blk00000063  (
    .I0(a[6]),
    .I1(a[4]),
    .I2(a[5]),
    .I3(a[7]),
    .O(\blk00000001/sig0000004b )
  );
  LUT4 #(
    .INIT ( 16'h1998 ))
  \blk00000001/blk00000062  (
    .I0(a[2]),
    .I1(NlwRenamedSignal_a[0]),
    .I2(NlwRenamedSignal_a[1]),
    .I3(a[3]),
    .O(\blk00000001/sig0000005f )
  );
  LUT4 #(
    .INIT ( 16'h4924 ))
  \blk00000001/blk00000061  (
    .I0(a[5]),
    .I1(a[4]),
    .I2(a[7]),
    .I3(a[6]),
    .O(\blk00000001/sig0000004c )
  );
  LUT4 #(
    .INIT ( 16'h6998 ))
  \blk00000001/blk00000060  (
    .I0(NlwRenamedSignal_a[1]),
    .I1(a[3]),
    .I2(NlwRenamedSignal_a[0]),
    .I3(a[2]),
    .O(\blk00000001/sig00000060 )
  );
  LUT4 #(
    .INIT ( 16'h4924 ))
  \blk00000001/blk0000005f  (
    .I0(NlwRenamedSignal_a[0]),
    .I1(a[3]),
    .I2(a[2]),
    .I3(NlwRenamedSignal_a[1]),
    .O(\blk00000001/sig00000061 )
  );
  LUT4 #(
    .INIT ( 16'h9DD4 ))
  \blk00000001/blk0000005e  (
    .I0(a[7]),
    .I1(a[6]),
    .I2(a[4]),
    .I3(a[5]),
    .O(\blk00000001/sig0000004e )
  );
  LUT4 #(
    .INIT ( 16'h9E18 ))
  \blk00000001/blk0000005d  (
    .I0(NlwRenamedSignal_a[1]),
    .I1(a[3]),
    .I2(a[2]),
    .I3(NlwRenamedSignal_a[0]),
    .O(\blk00000001/sig00000062 )
  );
  LUT4 #(
    .INIT ( 16'h93B2 ))
  \blk00000001/blk0000005c  (
    .I0(a[6]),
    .I1(a[4]),
    .I2(a[7]),
    .I3(a[5]),
    .O(\blk00000001/sig0000004f )
  );
  LUT3 #(
    .INIT ( 8'h62 ))
  \blk00000001/blk0000005b  (
    .I0(a[3]),
    .I1(a[2]),
    .I2(NlwRenamedSignal_a[1]),
    .O(\blk00000001/sig00000063 )
  );
  LUT4 #(
    .INIT ( 16'h9332 ))
  \blk00000001/blk0000005a  (
    .I0(a[4]),
    .I1(a[5]),
    .I2(a[7]),
    .I3(a[6]),
    .O(\blk00000001/sig00000050 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000059  (
    .I0(a[2]),
    .I1(a[3]),
    .O(\blk00000001/sig00000064 )
  );
  LUT4 #(
    .INIT ( 16'h4924 ))
  \blk00000001/blk00000058  (
    .I0(a[4]),
    .I1(a[7]),
    .I2(a[6]),
    .I3(a[5]),
    .O(\blk00000001/sig0000003e )
  );
  LUT4 #(
    .INIT ( 16'h9E18 ))
  \blk00000001/blk00000057  (
    .I0(a[5]),
    .I1(a[7]),
    .I2(a[6]),
    .I3(a[4]),
    .O(\blk00000001/sig0000003f )
  );
  LUT3 #(
    .INIT ( 8'h62 ))
  \blk00000001/blk00000056  (
    .I0(a[7]),
    .I1(a[6]),
    .I2(a[5]),
    .O(\blk00000001/sig00000040 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000055  (
    .I0(a[6]),
    .I1(a[7]),
    .O(\blk00000001/sig00000041 )
  );
  LUT4 #(
    .INIT ( 16'h6696 ))
  \blk00000001/blk00000054  (
    .I0(NlwRenamedSignal_a[1]),
    .I1(a[3]),
    .I2(NlwRenamedSignal_a[0]),
    .I3(a[2]),
    .O(p[3])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000053  (
    .I0(a[2]),
    .I1(NlwRenamedSignal_a[0]),
    .O(p[2])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000052  (
    .I0(\blk00000001/sig00000065 ),
    .I1(a[4]),
    .O(\blk00000001/sig00000032 )
  );
  MUXCY   \blk00000001/blk00000051  (
    .CI(NlwRenamedSig_OI_p[36]),
    .DI(\blk00000001/sig00000065 ),
    .S(\blk00000001/sig00000032 ),
    .O(\blk00000001/sig0000000c )
  );
  XORCY   \blk00000001/blk00000050  (
    .CI(NlwRenamedSig_OI_p[36]),
    .LI(\blk00000001/sig00000032 ),
    .O(p[4])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000004f  (
    .I0(\blk00000001/sig00000066 ),
    .I1(a[5]),
    .O(\blk00000001/sig0000003d )
  );
  MUXCY   \blk00000001/blk0000004e  (
    .CI(\blk00000001/sig0000000c ),
    .DI(\blk00000001/sig00000066 ),
    .S(\blk00000001/sig0000003d ),
    .O(\blk00000001/sig00000017 )
  );
  XORCY   \blk00000001/blk0000004d  (
    .CI(\blk00000001/sig0000000c ),
    .LI(\blk00000001/sig0000003d ),
    .O(p[5])
  );
  MUXCY   \blk00000001/blk0000004c  (
    .CI(\blk00000001/sig00000017 ),
    .DI(\blk00000001/sig00000067 ),
    .S(\blk00000001/sig00000042 ),
    .O(\blk00000001/sig00000028 )
  );
  XORCY   \blk00000001/blk0000004b  (
    .CI(\blk00000001/sig00000017 ),
    .LI(\blk00000001/sig00000042 ),
    .O(p[6])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000004a  (
    .I0(\blk00000001/sig00000068 ),
    .I1(\blk00000001/sig00000051 ),
    .O(\blk00000001/sig00000043 )
  );
  MUXCY   \blk00000001/blk00000049  (
    .CI(\blk00000001/sig00000028 ),
    .DI(\blk00000001/sig00000068 ),
    .S(\blk00000001/sig00000043 ),
    .O(\blk00000001/sig0000002b )
  );
  XORCY   \blk00000001/blk00000048  (
    .CI(\blk00000001/sig00000028 ),
    .LI(\blk00000001/sig00000043 ),
    .O(p[7])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000047  (
    .I0(\blk00000001/sig00000069 ),
    .I1(\blk00000001/sig00000052 ),
    .O(\blk00000001/sig00000044 )
  );
  MUXCY   \blk00000001/blk00000046  (
    .CI(\blk00000001/sig0000002b ),
    .DI(\blk00000001/sig00000069 ),
    .S(\blk00000001/sig00000044 ),
    .O(\blk00000001/sig0000002c )
  );
  XORCY   \blk00000001/blk00000045  (
    .CI(\blk00000001/sig0000002b ),
    .LI(\blk00000001/sig00000044 ),
    .O(p[8])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000044  (
    .I0(\blk00000001/sig0000005b ),
    .I1(\blk00000001/sig00000053 ),
    .O(\blk00000001/sig00000045 )
  );
  MUXCY   \blk00000001/blk00000043  (
    .CI(\blk00000001/sig0000002c ),
    .DI(\blk00000001/sig0000005b ),
    .S(\blk00000001/sig00000045 ),
    .O(\blk00000001/sig0000002d )
  );
  XORCY   \blk00000001/blk00000042  (
    .CI(\blk00000001/sig0000002c ),
    .LI(\blk00000001/sig00000045 ),
    .O(p[9])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000041  (
    .I0(\blk00000001/sig0000006a ),
    .I1(\blk00000001/sig00000054 ),
    .O(\blk00000001/sig00000046 )
  );
  MUXCY   \blk00000001/blk00000040  (
    .CI(\blk00000001/sig0000002d ),
    .DI(\blk00000001/sig0000006a ),
    .S(\blk00000001/sig00000046 ),
    .O(\blk00000001/sig0000002e )
  );
  XORCY   \blk00000001/blk0000003f  (
    .CI(\blk00000001/sig0000002d ),
    .LI(\blk00000001/sig00000046 ),
    .O(p[10])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000003e  (
    .I0(\blk00000001/sig0000006b ),
    .I1(\blk00000001/sig00000055 ),
    .O(\blk00000001/sig00000047 )
  );
  MUXCY   \blk00000001/blk0000003d  (
    .CI(\blk00000001/sig0000002e ),
    .DI(\blk00000001/sig0000006b ),
    .S(\blk00000001/sig00000047 ),
    .O(\blk00000001/sig0000002f )
  );
  XORCY   \blk00000001/blk0000003c  (
    .CI(\blk00000001/sig0000002e ),
    .LI(\blk00000001/sig00000047 ),
    .O(p[11])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000003b  (
    .I0(\blk00000001/sig00000059 ),
    .I1(\blk00000001/sig00000056 ),
    .O(\blk00000001/sig00000048 )
  );
  MUXCY   \blk00000001/blk0000003a  (
    .CI(\blk00000001/sig0000002f ),
    .DI(\blk00000001/sig00000059 ),
    .S(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000030 )
  );
  XORCY   \blk00000001/blk00000039  (
    .CI(\blk00000001/sig0000002f ),
    .LI(\blk00000001/sig00000048 ),
    .O(p[12])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000038  (
    .I0(\blk00000001/sig0000005a ),
    .I1(\blk00000001/sig0000004d ),
    .O(\blk00000001/sig00000049 )
  );
  MUXCY   \blk00000001/blk00000037  (
    .CI(\blk00000001/sig00000030 ),
    .DI(\blk00000001/sig0000005a ),
    .S(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000031 )
  );
  XORCY   \blk00000001/blk00000036  (
    .CI(\blk00000001/sig00000030 ),
    .LI(\blk00000001/sig00000049 ),
    .O(p[13])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000035  (
    .I0(\blk00000001/sig0000005b ),
    .I1(\blk00000001/sig00000057 ),
    .O(\blk00000001/sig00000033 )
  );
  MUXCY   \blk00000001/blk00000034  (
    .CI(\blk00000001/sig00000031 ),
    .DI(\blk00000001/sig0000005b ),
    .S(\blk00000001/sig00000033 ),
    .O(\blk00000001/sig0000000d )
  );
  XORCY   \blk00000001/blk00000033  (
    .CI(\blk00000001/sig00000031 ),
    .LI(\blk00000001/sig00000033 ),
    .O(p[14])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000032  (
    .I0(\blk00000001/sig0000005c ),
    .I1(\blk00000001/sig00000058 ),
    .O(\blk00000001/sig00000034 )
  );
  MUXCY   \blk00000001/blk00000031  (
    .CI(\blk00000001/sig0000000d ),
    .DI(\blk00000001/sig0000005c ),
    .S(\blk00000001/sig00000034 ),
    .O(\blk00000001/sig0000000e )
  );
  XORCY   \blk00000001/blk00000030  (
    .CI(\blk00000001/sig0000000d ),
    .LI(\blk00000001/sig00000034 ),
    .O(p[15])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000002f  (
    .I0(\blk00000001/sig0000005d ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig00000035 )
  );
  MUXCY   \blk00000001/blk0000002e  (
    .CI(\blk00000001/sig0000000e ),
    .DI(\blk00000001/sig0000005d ),
    .S(\blk00000001/sig00000035 ),
    .O(\blk00000001/sig0000000f )
  );
  XORCY   \blk00000001/blk0000002d  (
    .CI(\blk00000001/sig0000000e ),
    .LI(\blk00000001/sig00000035 ),
    .O(p[16])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk0000002c  (
    .I0(\blk00000001/sig0000005e ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig00000036 )
  );
  MUXCY   \blk00000001/blk0000002b  (
    .CI(\blk00000001/sig0000000f ),
    .DI(\blk00000001/sig0000005e ),
    .S(\blk00000001/sig00000036 ),
    .O(\blk00000001/sig00000010 )
  );
  XORCY   \blk00000001/blk0000002a  (
    .CI(\blk00000001/sig0000000f ),
    .LI(\blk00000001/sig00000036 ),
    .O(p[17])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000029  (
    .I0(\blk00000001/sig0000005f ),
    .I1(\blk00000001/sig0000004d ),
    .O(\blk00000001/sig00000037 )
  );
  MUXCY   \blk00000001/blk00000028  (
    .CI(\blk00000001/sig00000010 ),
    .DI(\blk00000001/sig0000005f ),
    .S(\blk00000001/sig00000037 ),
    .O(\blk00000001/sig00000011 )
  );
  XORCY   \blk00000001/blk00000027  (
    .CI(\blk00000001/sig00000010 ),
    .LI(\blk00000001/sig00000037 ),
    .O(p[18])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000026  (
    .I0(\blk00000001/sig00000060 ),
    .I1(\blk00000001/sig0000004e ),
    .O(\blk00000001/sig00000038 )
  );
  MUXCY   \blk00000001/blk00000025  (
    .CI(\blk00000001/sig00000011 ),
    .DI(\blk00000001/sig00000060 ),
    .S(\blk00000001/sig00000038 ),
    .O(\blk00000001/sig00000012 )
  );
  XORCY   \blk00000001/blk00000024  (
    .CI(\blk00000001/sig00000011 ),
    .LI(\blk00000001/sig00000038 ),
    .O(p[19])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000023  (
    .I0(\blk00000001/sig00000061 ),
    .I1(\blk00000001/sig0000004f ),
    .O(\blk00000001/sig00000039 )
  );
  MUXCY   \blk00000001/blk00000022  (
    .CI(\blk00000001/sig00000012 ),
    .DI(\blk00000001/sig00000061 ),
    .S(\blk00000001/sig00000039 ),
    .O(\blk00000001/sig00000013 )
  );
  XORCY   \blk00000001/blk00000021  (
    .CI(\blk00000001/sig00000012 ),
    .LI(\blk00000001/sig00000039 ),
    .O(p[20])
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk00000020  (
    .I0(\blk00000001/sig00000062 ),
    .I1(\blk00000001/sig00000050 ),
    .O(\blk00000001/sig0000003a )
  );
  MUXCY   \blk00000001/blk0000001f  (
    .CI(\blk00000001/sig00000013 ),
    .DI(\blk00000001/sig00000062 ),
    .S(\blk00000001/sig0000003a ),
    .O(\blk00000001/sig00000014 )
  );
  XORCY   \blk00000001/blk0000001e  (
    .CI(\blk00000001/sig00000013 ),
    .LI(\blk00000001/sig0000003a ),
    .O(p[21])
  );
  MUXCY   \blk00000001/blk0000001d  (
    .CI(\blk00000001/sig00000014 ),
    .DI(\blk00000001/sig00000063 ),
    .S(\blk00000001/sig0000003b ),
    .O(\blk00000001/sig00000015 )
  );
  XORCY   \blk00000001/blk0000001c  (
    .CI(\blk00000001/sig00000014 ),
    .LI(\blk00000001/sig0000003b ),
    .O(p[22])
  );
  MUXCY   \blk00000001/blk0000001b  (
    .CI(\blk00000001/sig00000015 ),
    .DI(\blk00000001/sig00000064 ),
    .S(\blk00000001/sig0000003c ),
    .O(\blk00000001/sig00000016 )
  );
  XORCY   \blk00000001/blk0000001a  (
    .CI(\blk00000001/sig00000015 ),
    .LI(\blk00000001/sig0000003c ),
    .O(p[23])
  );
  MUXCY   \blk00000001/blk00000019  (
    .CI(\blk00000001/sig00000016 ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig0000003e ),
    .O(\blk00000001/sig00000018 )
  );
  XORCY   \blk00000001/blk00000018  (
    .CI(\blk00000001/sig00000016 ),
    .LI(\blk00000001/sig0000003e ),
    .O(p[24])
  );
  MUXCY   \blk00000001/blk00000017  (
    .CI(\blk00000001/sig00000018 ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig0000003f ),
    .O(\blk00000001/sig00000019 )
  );
  XORCY   \blk00000001/blk00000016  (
    .CI(\blk00000001/sig00000018 ),
    .LI(\blk00000001/sig0000003f ),
    .O(p[25])
  );
  MUXCY   \blk00000001/blk00000015  (
    .CI(\blk00000001/sig00000019 ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig00000040 ),
    .O(\blk00000001/sig0000001a )
  );
  XORCY   \blk00000001/blk00000014  (
    .CI(\blk00000001/sig00000019 ),
    .LI(\blk00000001/sig00000040 ),
    .O(p[26])
  );
  MUXCY   \blk00000001/blk00000013  (
    .CI(\blk00000001/sig0000001a ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig00000041 ),
    .O(\blk00000001/sig0000001b )
  );
  XORCY   \blk00000001/blk00000012  (
    .CI(\blk00000001/sig0000001a ),
    .LI(\blk00000001/sig00000041 ),
    .O(p[27])
  );
  MUXCY   \blk00000001/blk00000011  (
    .CI(\blk00000001/sig0000001b ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig0000001d ),
    .O(\blk00000001/sig0000001c )
  );
  XORCY   \blk00000001/blk00000010  (
    .CI(\blk00000001/sig0000001b ),
    .LI(\blk00000001/sig0000001d ),
    .O(p[28])
  );
  MUXCY   \blk00000001/blk0000000f  (
    .CI(\blk00000001/sig0000001c ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig0000001f ),
    .O(\blk00000001/sig0000001e )
  );
  XORCY   \blk00000001/blk0000000e  (
    .CI(\blk00000001/sig0000001c ),
    .LI(\blk00000001/sig0000001f ),
    .O(p[29])
  );
  MUXCY   \blk00000001/blk0000000d  (
    .CI(\blk00000001/sig0000001e ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig00000021 ),
    .O(\blk00000001/sig00000020 )
  );
  XORCY   \blk00000001/blk0000000c  (
    .CI(\blk00000001/sig0000001e ),
    .LI(\blk00000001/sig00000021 ),
    .O(p[30])
  );
  MUXCY   \blk00000001/blk0000000b  (
    .CI(\blk00000001/sig00000020 ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig00000023 ),
    .O(\blk00000001/sig00000022 )
  );
  XORCY   \blk00000001/blk0000000a  (
    .CI(\blk00000001/sig00000020 ),
    .LI(\blk00000001/sig00000023 ),
    .O(p[31])
  );
  MUXCY   \blk00000001/blk00000009  (
    .CI(\blk00000001/sig00000022 ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig00000025 ),
    .O(\blk00000001/sig00000024 )
  );
  XORCY   \blk00000001/blk00000008  (
    .CI(\blk00000001/sig00000022 ),
    .LI(\blk00000001/sig00000025 ),
    .O(p[32])
  );
  MUXCY   \blk00000001/blk00000007  (
    .CI(\blk00000001/sig00000024 ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig00000027 ),
    .O(\blk00000001/sig00000026 )
  );
  XORCY   \blk00000001/blk00000006  (
    .CI(\blk00000001/sig00000024 ),
    .LI(\blk00000001/sig00000027 ),
    .O(p[33])
  );
  MUXCY   \blk00000001/blk00000005  (
    .CI(\blk00000001/sig00000026 ),
    .DI(NlwRenamedSig_OI_p[36]),
    .S(\blk00000001/sig0000002a ),
    .O(\blk00000001/sig00000029 )
  );
  XORCY   \blk00000001/blk00000004  (
    .CI(\blk00000001/sig00000026 ),
    .LI(\blk00000001/sig0000002a ),
    .O(p[34])
  );
  XORCY   \blk00000001/blk00000003  (
    .CI(\blk00000001/sig00000029 ),
    .LI(\blk00000001/sig0000004a ),
    .O(p[35])
  );
  GND   \blk00000001/blk00000002  (
    .G(NlwRenamedSig_OI_p[36])
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
