<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="l_a(7:0)" />
        <signal name="l_3(39:0)" />
        <signal name="l(39:0)" />
        <signal name="lp(36:0)" />
        <port polarity="Input" name="l_a(7:0)" />
        <port polarity="Output" name="l_3(39:0)" />
        <port polarity="Output" name="l(39:0)" />
        <port polarity="Output" name="lp(36:0)" />
        <blockdef name="l">
            <timestamp>2017-12-26T19:9:56</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="l_3">
            <timestamp>2017-12-26T19:9:45</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="l_ip">
            <timestamp>2017-12-26T19:9:24</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="l" name="XLXI_1">
            <blockpin signalname="l_a(7:0)" name="l_a(7:0)" />
            <blockpin signalname="l(39:0)" name="l_out2(39:0)" />
        </block>
        <block symbolname="l_3" name="XLXI_2">
            <blockpin signalname="l_a(7:0)" name="l_a(7:0)" />
            <blockpin signalname="l_3(39:0)" name="l_out1(39:0)" />
        </block>
        <block symbolname="l_ip" name="XLXI_3">
            <blockpin signalname="l_a(7:0)" name="a(7:0)" />
            <blockpin signalname="lp(36:0)" name="p(36:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1344" y="1200" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1360" y="848" name="XLXI_2" orien="R0">
        </instance>
        <branch name="l_a(7:0)">
            <wire x2="1104" y1="816" y2="816" x1="880" />
            <wire x2="1360" y1="816" y2="816" x1="1104" />
            <wire x2="1104" y1="816" y2="1168" x1="1104" />
            <wire x2="1344" y1="1168" y2="1168" x1="1104" />
            <wire x2="1104" y1="1168" y2="1472" x1="1104" />
            <wire x2="1376" y1="1472" y2="1472" x1="1104" />
        </branch>
        <iomarker fontsize="28" x="880" y="816" name="l_a(7:0)" orien="R180" />
        <instance x="1376" y="1392" name="XLXI_3" orien="R0">
        </instance>
        <branch name="l_3(39:0)">
            <wire x2="2368" y1="816" y2="816" x1="1744" />
        </branch>
        <iomarker fontsize="28" x="2368" y="816" name="l_3(39:0)" orien="R0" />
        <branch name="l(39:0)">
            <wire x2="2464" y1="1168" y2="1168" x1="1728" />
        </branch>
        <iomarker fontsize="28" x="2464" y="1168" name="l(39:0)" orien="R0" />
        <branch name="lp(36:0)">
            <wire x2="2096" y1="1472" y2="1472" x1="1952" />
        </branch>
        <iomarker fontsize="28" x="2096" y="1472" name="lp(36:0)" orien="R0" />
    </sheet>
</drawing>