-- Vhdl test bench created from schematic C:\Plis\l_3\l_sh.sch - Sat Dec 16 10:23:25 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY l_sh_l_sh_sch_tb IS
END l_sh_l_sh_sch_tb;
ARCHITECTURE behavioral OF l_sh_l_sh_sch_tb IS 

   COMPONENT l_sh
   PORT( l_a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          l_3	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          lp	:	OUT	STD_LOGIC_VECTOR (36 DOWNTO 0); 
          l	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0));
   END COMPONENT;

   SIGNAL l_a	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL l_3	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL lp	:	STD_LOGIC_VECTOR (36 DOWNTO 0);
   SIGNAL l	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
	constant clk_c : time :=10 ns;

BEGIN

   UUT: l_sh PORT MAP(
		l_a => l_a, 
		l_3 => l_3, 
		lp => lp, 
		l => l
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	l_A <="00000001";
	wait for clk_c;
	l_A <="00001111";
	wait for clk_c;	
	l_A <="11111111";
	wait for clk_c;
   END PROCESS;
END;
