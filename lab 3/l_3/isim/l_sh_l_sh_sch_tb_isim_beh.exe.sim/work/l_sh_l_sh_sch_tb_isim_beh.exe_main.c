/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *VL_P_2533777724;
char *IEEE_P_2592010699;
char *STD_STANDARD;
char *IEEE_P_1242562249;
char *UNISIM_P_0947159679;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000004134447467_2073120511_init();
    unisims_ver_m_00000000001784029001_4115003603_init();
    unisims_ver_m_00000000001784029001_1322938659_init();
    unisims_ver_m_00000000001784029001_3917634491_init();
    unisims_ver_m_00000000001784029001_0011281953_init();
    unisims_ver_m_00000000001784029001_0429172656_init();
    unisims_ver_m_00000000001784029001_1651864961_init();
    unisims_ver_m_00000000001784029001_3447087397_init();
    unisims_ver_m_00000000001784029001_4277751800_init();
    unisims_ver_m_00000000001784029001_0098786893_init();
    unisims_ver_m_00000000001784029001_2799381909_init();
    unisims_ver_m_00000000001784029001_1677110926_init();
    unisims_ver_m_00000000001784029001_2952069844_init();
    unisims_ver_m_00000000001784029001_0912996819_init();
    unisims_ver_m_00000000001784029001_1963876802_init();
    unisims_ver_m_00000000001784029001_1177212709_init();
    unisims_ver_m_00000000001108370118_0424564230_init();
    unisims_ver_m_00000000001108370118_2467748173_init();
    unisims_ver_m_00000000001108370118_2639295204_init();
    unisims_ver_m_00000000002444920515_2091797430_init();
    unisims_ver_m_00000000001773747898_0257217429_init();
    unisims_ver_m_00000000001773747898_2336946039_init();
    unisims_ver_m_00000000003848737514_1058825862_init();
    unisims_ver_m_00000000000909115699_2771340377_init();
    unisims_ver_m_00000000003317509437_1759035934_init();
    work_m_00000000004147660095_2101876659_init();
    work_m_00000000004153442281_3725161790_init();
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    unisim_p_0947159679_init();
    vl_p_2533777724_init();
    work_a_3837852192_3212880686_init();
    work_a_3564674210_3212880686_init();
    work_a_0026304961_3212880686_init();


    xsi_register_tops("work_a_0026304961_3212880686");
    xsi_register_tops("work_m_00000000004134447467_2073120511");

    VL_P_2533777724 = xsi_get_engine_memory("vl_p_2533777724");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");

    return xsi_run_simulation(argc, argv);

}
